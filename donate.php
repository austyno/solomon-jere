<?php session_start(); ?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>DONATION</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
    <link rel="icon" href="img/icon2.ico" />


    <!-- - - main style scripts -->
    <link rel='stylesheet' property='stylesheet' type="text/css" href="./bootstrap/bootstrap.min.css"/>
    <link rel='stylesheet' property='stylesheet' type="text/css" href="./qcreative/qcreative.css"/>
    <link rel='stylesheet' property='stylesheet' type="text/css" href="./qcreative/include_et.css"/>
    <link rel='stylesheet' property='stylesheet' type="text/css" href="dzsparallaxer/dzsparallaxer.css"/>
    <link rel="stylesheet" type="text/css" href="css/customstyle.css" />
    <script src="js/jquery.js"></script>
    <link rel='stylesheet' property='stylesheet' type="text/css" href="zfolio/zfolio.css"/>

    <!-- font inclusions
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:700italic,400,400italic,600,600italic,700,800' rel='stylesheet' property='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,400italic,700,900,900italic' rel='stylesheet' property='stylesheet' type='text/css'> -->
    <!-- font inclusions END-->



    <!-- preseter styles -->
    <link rel='stylesheet' property='stylesheet' type="text/css" href="dzstooltip/dzstooltip.css"/>

    <!-- preseter styles END-->


    <script class="mainoptions">

        // -- page options come here


        window.qcreative_options = {
            images_arr: ['img/backgrounds/21.jpg']  // -- the background
            ,enable_ajax: 'off' // -- enable "on" - ajax transition between pages / disable "off"
            ,bg_isparallax: 'on' // -- apply a parallax effect on scroll
//            ,new_bg_transition: "off" // -- if set to "off" then the initial background will remain
//            ,content_width: '1170' // -- apply a parallax effect on scroll
//            ,enable_native_scrollbar: 'on' // -- enable the native scrollbar on the whole site

//            ,responsive_menu_type: "select" // -- "custom" a custom forged, "select" a select menu for native mobile devices select menu

//            -- page options END here
        };
    </script>

    
    
    <style>
        #menu li a:hover{
            background-color:#0099cc;
        }
        
        #menu li ul li a{
             background-color:#0099cc;
        }
        #menu li ul li:hover > a{
            background-color:#0099cc;
        }
   
    </style>   
</head>


<body class="page-normal post-type-page content-align-center page-title-align-right page-title-style-2  menu-type-1 body-style-light">



<div class="main-container">


    <!-- this is where the main background will come, set image from above ( images_arr ) -->
    <div class="main-bg-con dzsparallaxer " data-options='{  mode_scroll: "fromtop" }'>
        <!--<figure class="main-bg dzsparallaxer&#45;&#45;target" style=""></figure>-->

        <!--<video src="video/under_the_sea_hd_stock_video.mp4" autoplay style="width: 100%;height: 100%; position: absolute;top:0;left:0; object-fit: cover;"></video>-->

    </div>




    <div class="the-content-con " style=";">



        <!-- page title // modify here -->
        <h1 class=""><i style="color:#0099cc"></i></h1>




        <div class="the-content">
            <!-- markup for blur // do not modify -->
            <div class="translucent-con ">
                <div class="translucent-bg  for-parallaxer"></div>
                <canvas class="translucent-canvas"></canvas>
                <div class="translucent-overlay"></div>
            </div>


            <div class="the-content-inner">



                <!-- section start -->

                <div class="the-content-sheet" style="background:url(img/backgrounds/21.jpg)">
                    
                    
                    <?php 
                        if(isset($_GET['mode'])){
                            $mode = $_GET['mode'];
                                if($mode == 'thanks'){
                                    echo'
                                        <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                We deeply appreciate your donation<strong> May the good LORD cause HIS face to shine upon you. Amen</strong>.
                            </div>
                                    ';
                                }
                        }
                    ?>
                    
                
                    <!-- section featured media -->
                    <div class="featured-media-con">
                        <div class="featured-media--image divimage lazyloading-transition-fade-in abt" data-src="img/about/aboutimg.jpg" style=" height:400px;"></div>
                    </div>

                    <div class="the-content-sheet-text" style="background:url(img/backgrounds/21.jpg)">




                        <!-- section title -->
                        <div class="row row-margin">
                            <div class="col-md-12">

                                <h2 style="color:white">DONATE</h2>
                            </div>
                        </div>
                        <div class="section-number"></div>

                        <?php include("includes/ref.php"); ?>
                        <!-- first row of icon boxes -->
                    <form class="form-overlay contact-form" style="background-color:grey" method="post" action="https://voguepay.com/pay/">
                            <!--<input type="hidden" name="ref" value=""-->
                            
                        <div class="row">
                            <!-- title and send button markup -->
                            <div class="col-md-3">
                                <div class="social-meta-con">

                                    <h4>SEND US A</h4>
                                    <span class="social-circle-con"><i class="fa fa-money"></i></span>
                                    <div class="clear"></div>

                                    <hr>
                                    <h3>Donation</h3>

                                </div>
                            </div>
                           
                            <!-- title and send button markup END-->


                            <div class="col-md-9">

                                <!-- name and email form markup -->
                                <div class="row smaller-padding">
                                    
                                    
                                        <div class="col-md-6">

                                        <input type="text" name="total"  class="input-for-email" placeholder="Amount..."/>
                                        </div>
                                   
                                </div>

                                 
                                <div class="clear"></div>
                                
                                <div width="120px">
                                
                                    
                                
                                Choose Currency<br />
                                <select name="cur" style="width:120px">
                                <option value="NGN">NGN - Nigerian Naira</option>
                                <option value="USD" selected>USD - US Dollar</option>
                                <option value="ZAR">ZAR - South African Rand</option>
                                </select><br />
                                <input type="hidden" name="v_merchant_id" value="5295-0052655" />
                                <input type="hidden" name="success_url" value="http://solomonjere.com" />
                                <input type="hidden" name="memo" value="Donation to solomon jere" />
                                <input type="image" src="http://voguepay.com/images/buttons/donate_blue.png" alt="PAY" />
                                
                                </div>
                                
                                
                                
                                
                                
                                 
                                <!-- message area markup 
                                <input type='hidden' name='v_merchant_id' value='demo' />
                                <input type='hidden' name='merchant_ref' value='234-567-890' />
                                <input type='hidden' name='memo' value='donation' />
                                <input type='hidden' name='item_1' value='donation' />
                                <input type='hidden' name='description_1' value='donation' />
                                <input type='hidden' name='notify_url' value='http://solomonjere.com/notification.php' />
                                <input type='hidden' name='success_url' value='http://solomonjere.com/notification.php' />
                                <input type='hidden' name='fail_url' value='http://solomonjere.com/notification.php' />


                                <button type="submit" name="sbtn" class="btn btn-primary"><img src='http://voguepay.com/images/buttons/donate_grey.png'></button>-->

                                <!-- this is the feedback form when the message has been sent -->
                                
                            </div>
                           
                        </div>
                            
                    </form>
                </div>
                </div>
            <!-- end first section -->



            <!-- section start -->
            
            <!-- section END -->



            <!-- section start -->
            


            <!-- section start -->
             

       
            
            <!-- section END -->




            <!-- section start -->
            
<div class="the-content-sheet">

                    <div class="featured-media-con social-block sc-social-block">

                        <div class="featured-media--image divimage" style="background-image: url(img/secondary_content/social.jpg);background-color:#0099cc"></div>
                        <div class="semi-black-overlay opaque"></div>
                        <div class="row">

                            <div class="col-md-3">
                                <a href="#" class="social-meta-con">

                                    <h4>LIKE US ON</h4>
                                    <span class="social-circle-con"><i class="fa fa-facebook-square"></i></span>
                                    <div class="clear"></div>

                                    <hr>
                                    <h3>Facebook</h3>

                                </a>
                            </div>

                            <div class="col-md-3">
                                <a href="#" class="social-meta-con">

                                    <h4>FOLLOW US ON</h4>
                                    <span class="social-circle-con"><i class="fa fa-twitter"></i></span>
                                    <div class="clear"></div>

                                    <hr>
                                    <h3>Twitter</h3>

                                </a>
                            </div>

                            <div class="col-md-3">
                                <a href="#" class="social-meta-con">

                                    <h4>PIN THIS ON</h4>
                                    <span class="social-circle-con"><i class="fa fa-pinterest"></i></span>
                                    <div class="clear"></div>

                                    <hr>
                                    <h3>Pinterest</h3>

                                </a>
                            </div>

                            <div class="col-md-3">
                                <a href="#" class="social-meta-con">

                                    <h4>APPRECIATE US ON</h4>
                                    <span class="social-circle-con"><i class="fa fa-google-plus"></i></span>
                                    <div class="clear"></div>

                                    <hr>
                                    <h3>Google+</h3>

                                </a>
                            </div>
                        </div>
                    </div>
                </div>



        </div>
    </div>
        </div>
    </div>






    <!-- this block is the navigation -->
    <nav class="qcreative--nav-con">
        <div class="translucent-con " style="">
            <div class="translucent-bg for-parallaxer"></div>
            <canvas id="tr" class="translucent-canvas"></canvas>
            <div class="translucent-overlay"></div>
        </div>

        <!-- modify logo from here -->
        <div class="logo-con">
            <div class="the-logo" style="width: 183px; background-image:url(img/logo3.png);"></div>
        </div>

        <!-- this block will be populated by gallery thumbnail navigation -->
        <ul id="menu" class="the-actual-nav">
            <li class="menu-item"><a href="index.php">Home</a></li>
            <li class="menu-item"><a href="about.php">About Us</a></li>
            
            <li class="menu-item"><a href="contact.php">contact us</a></li>
             <li class="menu-item"><a href="msg.php">My Thoughts</a></li>
             <li class="menu-item"><a href="songs.php">Songs</a></li>
            <li class="menu-item"><a href="gallery.php">gallery</a></li>
            <li class="menu-item"><a href="vids.php">video</a></li>
                    
            
            
            
            <li id="don" class="menu-item active"><a href="donate.php"><img src="img/donate.png" width="150px"></a></li>
            
           
        </ul>

        <div class="nav-social-con">
            <p class="social-icons">
                <a href="#"><i class="fa fa-facebook-square"></i></a>
                <a href="#"><i class="fa fa-behance"></i></a>
                <a href="#"><i class="fa fa-pinterest"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-linkedin-square"></i></a>
            </p>
            <p class="copyright-text">COPYRIGHT &copy; AppNov Tech 2017</p>
        </div>

    </nav>
<!-- end navigation -->


    <!-- preloader area -->
    <div class="preloader-con">
       <i class="fa fa-spin fa-spinner"></i>-->
        <div class="cube-preloader"></div>
    </div>




<!-- lightbox settings -->
<script class="zoombox-settings">
    window.init_zoombox_settings = {
        settings_zoom_doNotGoBeyond1X:'off'
        ,design_skin:'skin-whitefull'
        ,settings_enableSwipe:'on'
        ,settings_enableSwipeOnDesktop:'on'
        ,settings_galleryMenu:'dock'
        ,settings_useImageTag:'on'
        ,settings_paddingHorizontal : '0'
        ,settings_paddingVertical : '0'
    };

</script>



<!--
- - next will come the loaded scripts
-->
<link rel='stylesheet' property='stylesheet' type="text/css" href="dzsselector/dzsselector.css"/>
<script src="dzsselector/dzsselector.js" type="text/javascript"></script>

<script src="qcreative/qcreative.js"></script>
<link href='advancedscroller/plugin.css' rel='stylesheet' property='stylesheet' type='text/css'>
<script src="advancedscroller/plugin.js"></script>
<link href='fontawesome/font-awesome.min.css' rel='stylesheet' property='stylesheet' type='text/css'>
<link rel='stylesheet' property='stylesheet' type="text/css" href="zoombox/zoombox.css"/>
<script src="zoombox/zoombox.js" type="text/javascript"></script>
<script src="zfolio/zfolio.js" type="text/javascript"></script>
<script src="zfolio/jquery.isotope.min.js" type="text/javascript"></script>
<script src="dzsparallaxer/dzsparallaxer.js" type="text/javascript"></script>
<script src="dzstooltip/dzstooltip.js" type="text/javascript"></script>
<link rel='stylesheet' property='stylesheet' type="text/css" href="dzsprogressbars/dzsprogressbars.css"/>
<script src="dzsprogressbars/dzsprogressbars.js" type="text/javascript"></script>
    
<link rel='stylesheet' property='stylesheet' type="text/css" href="dzstabsandaccordions/dzstabsandaccordions.css"/>
<script src="dzstabsandaccordions/dzstabsandaccordions.js"></script>
    
    
    <link rel='stylesheet' type="text/css" href="audioplayer/audioplayer.css"/>
<script src="audioplayer/audioplayer.js" type="text/javascript"></script>
    
    
<script src="dzsscroller/scroller.js" type="text/javascript"></script>
<link rel='stylesheet' property='stylesheet' type="text/css" href="dzsscroller/scroller.css"/>
<link rel='stylesheet' property='stylesheet' type="text/css" href="videogallery/vplayer.css"/>
<script type="text/javascript" src="videogallery/vplayer.js"></script>

<!-- customizer scripts --><!-- customizer scripts END -->


</body>
</html>
 