<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="img/icon2.ico" />


    <!-- - - main style scripts -->
    <link rel='stylesheet' property='stylesheet' type="text/css" href="./bootstrap/bootstrap.min.css"/>
    <link rel='stylesheet' property='stylesheet' type="text/css" href="./qcreative/qcreative.css"/>
     <link rel="stylesheet" type="text/css" href="css/customstyle.css" />
     <link rel="stylesheet" type="text/css" href="css/social-share-kit.css" />
    <script src="js/jquery.js"></script>

    <!-- font inclusions 
   <link href='http://fonts.googleapis.com/css?family=Open+Sans:700italic,400,400italic,600,600italic,700,800' rel='stylesheet' property='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,400italic,700,900,900italic' rel='stylesheet' property='stylesheet' type='text/css'>-->
    <!-- font inclusions END-->




    <script class="mainoptions">

        // -- page options come here

//'img/4.jpg',
        window.qcreative_options = {
            images_arr: ['img/home/sl5.jpg','img/home/rsz_24.jpg','img/home/sl5.jpg','img/home/rsz_24.jpg'] // -- the background
            ,enable_ajax: 'off' // -- enable "on" - ajax transition between pages / disable "off"
            ,bg_isparallax: 'on' // -- apply a parallax effect on scroll
            ,bg_slideshow_time: '15' // -- set a slideshow time in seconds
//            ,bg_transition: 'fade' // -- set a slideshow time in seconds


            // -- page options END here

        };
    </script>
    <!-- preseter styles -->
    <link rel='stylesheet' property='stylesheet' type="text/css" href="dzstooltip/dzstooltip.css"/>

    <!-- preseter styles END-->
    
    <style>
        
        #menu li a:hover{
            background-color:#0099cc;
        }
        
        #menu li ul li a{
             background-color:#0099cc;
        }
        #menu li ul li:hover > a{
            background-color:#0099cc;
        }
        .active > a{
            background-color: #0099cc;; 
        }
        #don{
           font-style: italic;
            
        }
        #don a{
            color: #0099cc;
        
        }
    </style>
    
    <script type="text/javascript">
    $(document).ready(function(){
        $("#myModal").modal('show');
    });
</script>
    
</head>
<body class="page-homepage post-type-page content-align-center page-title-align-right page-title-style-1 menu-type-1">
    
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=1422300894730590";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    


<!-- markup for background // do not modify -->
<div class="main-bg-con">
    <figure class="main-bg" style=""></figure>
</div>

<div class="main-container">


    <div class="the-content-con for-homepage">
        

        <!-- descriptions for gallery items container -->
        <div class="main-gallery--descs" style="height: 175px;">

            <!-- description for first item start -->
            <!--<div class="main-gallery--desc">
                <div class="desc-inner">

                    <!-- blur markup 
                    <div class="translucent-con">
                        <div class="translucent-bg" id="desc1-translucent-bg"></div>
                        <canvas class="translucent-canvas"></canvas>
                        <div class="translucent-overlay"></div>
                    </div>
                    <!-- blur markup END 

                        <span class="big-desc" style="height:auto">VISION<br>focus on<br>the salvation path of Jesus.</span>
                    <span class="big-number" style="color:skyblue">01</span>
                    <!--<span style="display: block; font-size: 16px; background-color: #ffffff; position:relative; text-align: left; padding: 8px 15px; ">To focus on the salvation path of Jesus Christ in song and service as we await His second return.</span>
                </div>
            </div>
            <!-- description END -->


            <!-- description for second item // style2 // start -->
            <div class="main-gallery--desc style2">
                <div class="desc-inner">

                    <!-- blur markup -->
                    <div class="translucent-con">
                        <div class="translucent-bg"></div>
                        <canvas class="translucent-canvas"></canvas>
                        <div class="translucent-overlay"></div>
                    </div>
                    <!-- blur markup END -->

                <span class="big-desc"><a class="twitter-timeline" data-lang="en" data-width="250" data-height="500" href="https://twitter.com/drsolomonmusic">Tweets by Dr Solomon</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script></span>
                    <span class="big-number" style="color:skyblue"></span>
                </div>
            </div>
            <!-- description END -->


            <!-- description for third item start -->
            <div class="main-gallery--desc">
                <div class="desc-inner">

                    <!-- blur markup -->
                    <div class="translucent-con">
                        <div class="translucent-bg"></div>
                        <canvas class="translucent-canvas"></canvas>
                        <div class="translucent-overlay"></div>
                    </div>
                    <!-- blur markup END -->

                        <span class="big-desc"></span>
                    <span class="big-number" style="color:skyblue"><iframe src="http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fsolomojere&amp;width=490&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=true&amp;header=true&amp;height=435" scrolling="yes" frameborder="0" style="border:none; overflow:hidden; width:490px; height:490px; background: white; float:left; " allowTransparency="true"></iframe></span>
                </div>
            </div>
            <!-- description END -->


            <!-- description for fourth item start -->
            <div class="main-gallery--desc style2">
                <div class="desc-inner">

                    <!-- blur markup -->
                    <div class="translucent-con">
                        <div class="translucent-bg"></div>
                        <canvas class="translucent-canvas"></canvas>
                        <div class="translucent-overlay"></div>
                    </div>
                    <!-- blur markup END -->

                        <span class="big-desc">

                                Love The Lord More

                    </span>
                    <span class="big-number" style="color:skyblue"></span>
                </div>
            </div>
            <!-- description END -->
  
            <div class="main-gallery--desc style2">
                <div class="desc-inner">

                    <!-- blur markup -->
                    <div class="translucent-con">
                        <div class="translucent-bg"></div>
                        <canvas class="translucent-canvas"></canvas>
                        <div class="translucent-overlay"></div>
                    </div>
                    <!-- blur markup END -->

                        <span class="big-desc">

                            The Only Living God 

                    </span>
                    <span class="big-number" style="color:skyblue"></span>
                </div>
            </div>
            
            
            
            
            
            
            
            
            
            
        </div>
        <!-- descriptions for gallery items container END-->

        <!-- buttons for next / previous markup -->
        <!--<div class="main-gallery-buttons-con style2">

            <div class="prev-btn-con">
                <span class="btn-text">PREVIOUS SLIDE</span>
                <figure>
                    <i class="fa fa-angle-left"></i>
                </figure>
            </div>
            <div class="next-btn-con">
                <span class="btn-text">NEXT SLIDE</span>
                <figure>
                    <i class="fa fa-angle-right"></i>
                </figure>
            </div>
        </div>
        <!-- buttons for next / previous markup END 
    </div>-->



    <!-- this block is the navigation -->
    <nav class="qcreative--nav-con">
        <div class="translucent-con " style="">
            <div class="translucent-bg for-parallaxer"></div>
            <canvas id="tr" class="translucent-canvas"></canvas>
            <div class="translucent-overlay"></div>
        </div>

        <!-- modify logo from here -->
        <div class="logo-con">
            <div class="the-logo" style="width: 183px; background-image:url(img/logo3.png);"></div>
        </div>
        
        <!-- this block will be populated by gallery thumbnail navigation -->
        <ul id="menu" class="the-actual-nav">
            <li class="menu-item active"><a href="index.php">Home</a></li>
            <li class="menu-item"><a href="about.php">About Us</a></li>
            
            <li class="menu-item"><a href="contact.php">contact us</a></li>
             <li class="menu-item"><a href="msg.php">words of wisdom</a></li>
             <li class="menu-item"><a href="songs.php">Songs</a></li>
            <li class="menu-item"><a href="gallery.php">gallery</a></li>
            <li class="menu-item"><a href="vids.php">video</a></li>
            <li id="don" class="menu-item"><a href="donate.php"><img src="img/donate.png" width="150px"></a></li>
            
           
        </ul>
        <div class="nav-social-con">
            <p class="social-icons">
                <a href="https://www.facebook.com/solomojere"><i class="fa fa-facebook-square"></i></a>
                <a href="https://www.youtube.com/channel/UC4W3y9-fbPINR7FG8LINPoQ" ><i class="fa fa-youtube"></i></a>
                <a href="https://www.pinterest.com/solomonjere/" target="_blank"><i class="fa fa-pinterest"></i></a>
                <a href="https://twitter.com/drsolomonmusic"><i class="fa fa-twitter"></i></a>
                <a href="https://www.instagram.com/solomonjeremusic"><i class="fa fa-instagram"></i></a>
                <!--<a href="#"><i class="fa fa-linkedin-square"></i></a>-->
            </p>
            <p class="copyright-text">COPYRIGHT &copy; Solomon Jere</p>
        </div>

    </nav>
<!-- end navigation -->



    <!-- preloader area -->
    <div class="preloader-con">
        <!--<i class="fa fa-spin fa-spinner"></i>
        <div class="cube-preloader"></div>-->
    </div>
</div>


<!--
- - next will come the loaded scripts
-->


<div class="modal fade" id="myModal" style="background-color:silver;height:400px;width:50%;margin:auto;border-radius:30px">
      <div class="modal-header">
        <a class="close" data-dismiss="modal">x</a>
          <h5 align="center" style="color:white"><i>Buy My Albums</i></h5>
      </div>
      <div class="modal-body">
          
        <a href="http://itunes.apple.com/album/id1234392813?ls=1&app=itunes" target="_blank"><img src="img/adam_ndi_hava.jpg"></a>
        <a href="https://itunes.apple.com/us/album/an-ugly-tree/id1234388293" target="_blank"><img src="img/ugly_tree.jpg"></a>
      </div>
      <div class="modal-footer">
          
          <div class="ssk-group" style="float:left">
   
    <a href="" class="ssk ssk-icon ssk-twitter"></a>
    
    <a href="" class="ssk ssk-icon ssk-pinterest"></a>
    <a href="" class="ssk ssk-icon ssk-instagram"></a>
</div>
          <iframe src="https://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fitunes.apple.com%2Falbum%2Fid1234392813%3Fls%3D1%26app%3Ditunes&width=450&layout=standard&action=like&size=small&show_faces=false&share=true&height=35&appId" width="450" height="35" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
          
        <a href="#" class="btn" data-dismiss="modal" style="float:right">Close</a>
      </div>
</div>
















<link rel='stylesheet' property='stylesheet' type="text/css" href="dzsselector/dzsselector.css"/>
<script src="dzsselector/dzsselector.js" type="text/javascript"></script>
<script src="advancedscroller/plugin.js"></script>

<script src="qcreative/qcreative.js"></script>
<link href='advancedscroller/plugin.css' rel='stylesheet' property='stylesheet' type='text/css'>
<link href='fontawesome/font-awesome.min.css' rel='stylesheet' property='stylesheet' type='text/css'>
<script src="dzsparallaxer/dzsparallaxer.js" type="text/javascript"></script>
<script src="dzsscroller/scroller.js" type="text/javascript"></script>
<link rel='stylesheet' property='stylesheet' type="text/css" href="dzsscroller/scroller.css"/>
<script src="bootstrap/bootstrap.min.js"></script>
<!-- customizer scripts --><!-- customizer scripts END -->
</body>
</html>