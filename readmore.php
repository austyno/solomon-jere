<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Messages</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="icon" href="img/icon2.ico" />



    <!-- - - main style scripts -->

    <link rel='stylesheet' property='stylesheet' type="text/css" href="./bootstrap/bootstrap.min.css"/>
    <link rel='stylesheet' property='stylesheet' type="text/css" href="./qcreative/qcreative.css"/>
    <link rel='stylesheet' property='stylesheet' type="text/css" href="./qcreative/include_et.css"/>
    <link rel='stylesheet' property='stylesheet' type="text/css" href="dzsparallaxer/dzsparallaxer.css"/>
    <script src="js/jquery.js"></script>
    <script src="tinymce/js/tinymce/tinymce.min.js"></script>

    <link rel='stylesheet' property='stylesheet' type="text/css" href="dzstooltip/dzstooltip.css"/>

    <!-- font inclusions
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:700italic,400,400italic,600,600italic,700,800' rel='stylesheet' property='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,400italic,700,900,900italic' rel='stylesheet' property='stylesheet' type='text/css'> -->
    <!-- font inclusions END-->


    <link rel='stylesheet' property='stylesheet' type="text/css" href="zfolio/zfolio.css"/>


    <script class="mainoptions">

        // -- page options come here


        window.qcreative_options = {
            images_arr: ['img/backgrounds/21.jpg']  // -- the background
            ,enable_ajax: 'off' // -- enable "on" - ajax transition between pages / disable "off"
            ,bg_isparallax: 'on' // -- apply a parallax effect on scroll


        };
        // -- page options END here
    </script>
    <!-- preseter styles -->
    <link rel='stylesheet' property='stylesheet' type="text/css" href="dzstooltip/dzstooltip.css"/>

    <!-- preseter styles END-->
    
    <style>
        
        #menu li a:hover{
            background-color:#0099cc;
        }
        
        #menu li ul li a{
             background-color:#0099cc;
        }
        #menu li ul li:hover > a{
            background-color:#0099cc;
        }
        .active > a{
            background-color: #0099cc;; 
        }
        #don{
           font-style: italic;
            
        }
        #don a{
            color: #0099cc;
        
        }
        #sep a:hover{
             background-color:#0099cc;
        }
        #more{
            background-color:gray;
        }
        #more:hover{
            background-color:#0099cc;
        }
        
        
    </style>
    
    <script type="text/javascript">
    tinymce.init({
            selector: '#commentText'
        });
  </script>
      
</head>
<body class="page-normal post-type-page content-align-center page-title-align-right page-title-style-2 menu-type-1">


<!-- this is where the main background will come, set image from above ( images_arr ) -->
<div class="main-bg-con dzsparallaxer " data-options='{  mode_scroll: "fromtop" }'>
    <figure class="main-bg dzsparallaxer--target" style=""></figure>
</div>


<div class="main-container">




    <div class="the-content-con">

        <!-- page title -->
        <h1 class=""></h1>


        <div class="the-content">

            <!-- markup for blur // do not modify -->
            <div class="translucent-con ">
                <div class="translucent-bg  for-parallaxer"></div>
                <canvas class="translucent-canvas"></canvas>
                <div class="translucent-overlay"></div>
            </div>





            <div class="the-content-inner">
                <!-- section start -->
         

                    <!-- section END -->

                    <!-- section start -->
                    <div class="the-content-sheet">
                            
                            <?php
                                include_once("includes/dbconfig.php");
                            
                                
                            $url = $_SERVER["REQUEST_URI"];
                            
                           $query_str = parse_url($url, PHP_URL_QUERY);
                            parse_str($query_str, $query_params);
                            //print_r($query_params);
                            
                           $id = $query_params['msgid'];
                        
                               
                                $qry = "select * from msg where msg_id ='$id' limit 1";
                                $rslt = mysql_query($qry);
                                $rs = mysql_fetch_assoc($rslt);
                            
                                    do{
                                        if($rs['msg_img'] !== ""){
                                            echo '<div class="featured-media-con">
                                <img alt="image" class="fullwidth" src="img/'.$rs['msg_img'].'">
                            </div>
                            <div class="post-content-con">

                                <h3 style=";">'.$rs['msg_title'].'</h3>
                                <div class="post-meta">
                                    Posted on '.$rs['msg_date_pub'].' | 54 <a href="#comments" style="color:blue">comments</a>
                                </div>
                                <hr class="extend-margin-30">
                                <p>'.$rs['msg_content'].'</p>
                            </div>
                            <div class="post-meta-below"  style="margin-left:30px;margin-top:10px
                            ">

                                <a class="social-icon" href="#"  onclick=\'window.qcre_open_social_link("http://www.facebook.com/sharer.php?u={{replaceurl}}"); return false;\'><i class="fa fa-facebook-square"></i><span class="the-tooltip">SHARE ON FACEBOOK</span></a>


                                <a class="social-icon" href="#" onclick=\'window.qcre_open_social_link("http://twitter.com/share?url={{replaceurl}}&amp;text=Check this out!&amp;via=campaignmonitor&amp;related=yarrcat"); return false;\'><i class="fa fa-twitter"></i><span class="the-tooltip">SHARE ON TWITTER</span></a>


                                <a class="social-icon" href="#" onclick=\'window.qcre_open_social_link("https://plus.google.com/share?url={{replaceurl}}"); return false;\'><i class="fa fa-google-plus-square"></i><span class="the-tooltip">SHARE ON GOOGLE PLUS</span></a>


                                <a class="social-icon" href="#" onclick=\'window.qcre_open_social_link("https://www.linkedin.com/shareArticle?mini=true&url=mysite&title=Check%20this%20out%20{{replaceurl}}&summary=&source={{replaceurl}}"); return false;\'><i class="fa fa-linkedin"></i><span class="the-tooltip">SHARE ON LINKEDIN</span></a>

                                <a class="social-icon" href="#" onclick=\'window.qcre_open_social_link("http://pinterest.com/pin/create/button/?url={{replaceurl}}&amp;text=Check this out!&amp;via=campaignmonitor&amp;related=yarrcat"); return false;\'><i class="fa fa-pinterest"></i><span class="the-tooltip">SHARE ON PINTEREST</span></a>


                                <div class="post-meta-below--meta" style="padding-bottom:15px;">
                                    <div class="separator-line"></div>
                                    
                                    <div class="clear"></div>
                                    
                                    
                                    <div class="clear"></div>
                                    <div class="meta-left">AUTHOR</div>
                                    <div class="meta-right"><strong><i>'.$rs['msg_author'].'</i></strong></div>
                                    <div class="clear"></div>
                                </div>


                            </div>';
                                            
                                        }else{
                                            echo '<div class="featured-media-con">
                                
                            </div>
                            <div class="post-content-con">

                                <h3 style=";">'.$rs['msg_title'].'</h3>
                                <div class="post-meta">
                                    Posted on '.$rs['msg_date_pub'].' | 54 <a href="#comments" style="color:blue">comments</a>
                                </div>
                                <hr class="extend-margin-30">
                                <p>'.$rs['msg_content'].'</p>
                            </div>
                            <div class="post-meta-below"  style="margin-left:30px;margin-top:10px
                            ">

                                <a class="social-icon" href="#"  onclick=\'window.qcre_open_social_link("http://www.facebook.com/sharer.php?u={{replaceurl}}"); return false;\'><i class="fa fa-facebook-square"></i><span class="the-tooltip">SHARE ON FACEBOOK</span></a>


                                <a class="social-icon" href="#" onclick=\'window.qcre_open_social_link("http://twitter.com/share?url={{replaceurl}}&amp;text=Check this out!&amp;via=campaignmonitor&amp;related=yarrcat"); return false;\'><i class="fa fa-twitter"></i><span class="the-tooltip">SHARE ON TWITTER</span></a>


                                <a class="social-icon" href="#" onclick=\'window.qcre_open_social_link("https://plus.google.com/share?url={{replaceurl}}"); return false;\'><i class="fa fa-google-plus-square"></i><span class="the-tooltip">SHARE ON GOOGLE PLUS</span></a>


                                <a class="social-icon" href="#" onclick=\'window.qcre_open_social_link("https://www.linkedin.com/shareArticle?mini=true&url=mysite&title=Check%20this%20out%20{{replaceurl}}&summary=&source={{replaceurl}}"); return false;\'><i class="fa fa-linkedin"></i><span class="the-tooltip">SHARE ON LINKEDIN</span></a>

                                <a class="social-icon" href="#" onclick=\'window.qcre_open_social_link("http://pinterest.com/pin/create/button/?url={{replaceurl}}&amp;text=Check this out!&amp;via=campaignmonitor&amp;related=yarrcat"); return false;\'><i class="fa fa-pinterest"></i><span class="the-tooltip">SHARE ON PINTEREST</span></a>


                                <div class="post-meta-below--meta" style="padding-bottom:15px;">
                                    <div class="separator-line"></div>
                                    
                                    <div class="clear"></div>
                                    
                                    
                                    <div class="clear"></div>
                                    <div class="meta-left">AUTHOR</div>
                                    <div class="meta-right"><strong><i>'.$rs['msg_author'].'</i></strong></div>
                                    <div class="clear"></div>
                                </div>


                            </div>';
                                        }
                                               
                                
                                        
                                        
                                    }while($rs = mysql_fetch_assoc($rslt));
                            
                            ?>
                         
                        </div>
                
                    
                        
                    <div class="the-content-sheet">
                            <div id="replies" class="itemComments blog-comments">




                                <ul class="itemCommentsList" id="comments">
                                    
                                    <?php
                                      include_once("includes/dbconfig.php");
                                        
                                         $url = $_SERVER["REQUEST_URI"];
                            
                                        $query_str = parse_url($url, PHP_URL_QUERY);
                                        parse_str($query_str, $query_params);
                                        //print_r($query_params);
                            
                                        $id = $query_params['msgid'];
                                        
                                        $qry2 = "select * from comments where cmt_msg_id = $id order by cmt_date DESC";
                                        $rslt2 = mysql_query($qry2);
                                        $rs2 = mysql_fetch_assoc($rslt2);
                                        $found = mysql_num_rows($rslt2);
                                    
                                        if($found){
                                            
                                            
                                            do{
                                                
                                               echo '<li class="comment byuser comment-author-admin bypostauthor even thread-even depth-1" id="li-comment-7">
                                        <div id="comment-7">
                                            <div class="comment-meta" style="width:200px">
                                                <div class="comment-thumb" style="background-image: url(img/blog/other/comment1.jpg);"></div>
                                                <div class="comment-other-meta">
                                                    <h4>'.$rs2['cmt_author'].'</h4>
                                                    <span class="comment-time">'.$rs2['cmt_date'].'</span>
                                                </div>
                                            </div>
                                            <div class="comment-body">'.
                                                        $rs2['cmt_content'].'

                                                <div class="comment-reply">

                                                </div><!-- .reply -->
                                            </div>
                                            <div class="comment-right-meta">
                                                <span class="meta-comment-reply"><a class="comment-reply-link" href="#" onclick="" aria-label="#">Reply</a></span>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </li>'; 
                                                
                                            }while($rs2 = mysql_fetch_assoc($rslt2));
                                              
                                           echo '<a href="#" id="more" class="btn-load-more-comments">LOAD MORE COMMENTS</a>';
                                        }else{
                                            echo '<h6><i class="fa fa-comments-o fa-2x" style=""> would you like to be the first to comment on this article</i></h6>';
                                        }
                                    
                                    
                                    
                                    
                                    
                                    if(isset($_POST['sbtn'])){
                                        $user = $_POST['userName'];
                                        $email = $_POST['commentEmail'];
                                        $comment = $_POST['comment'];
                                        
                                         $id = $query_params['msgid_'];
                                        
                                        $Sql = "insert into comments (cmt_id,cmt_msg_id,cmt_content,cmt_date,cmt_email,cmt_author)values(null,'$id','$comment',NOW(),'$email','$user')";
                                        
                                        $result = mysql_query($Sql);
                                            if($result){
                                                echo '
                                                    <div class="alert alert-success alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Comment has been saved successfuly.
                                                    </div>';
                                            }  
                                        
                                    }
                                    
                                    
                                    ?>
                                    
                                       </ul>
                               

                               <div class="row row-margin row-element-divider">
                                    <div class="col-md-12">
                                        <div class="style-black style-3 divider"></div>
                                   </div>
                            </div>

                                <div class="blog-reply">
                                    <div id="respond" class="comment-respond">
                                        <h3 id="reply-title" class="comment-reply-title">Leave a comment</h3>


                                        <form  method="post" id="itemCommentsForm" class="comment-form" action="">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="text" name="userName" id="userName" class="inputbox"  value="Name..." size="30" onfocus="if(this.value=='Name...') this.value='';" onblur="if(this.value=='') this.value='Name...';">
                                                </div>
                                                <div class="col-md-6">

                                                    <input type="text" name="commentEmail" id="commentEmail" class="inputbox"  value="Email..." size="30" onfocus="if(this.value=='Email...') this.value='';" onblur="if(this.value=='') this.value='Email...';">
                                                </div>
                                            </div>
                                                


                                            <div class="row">
                                                <div class="col-md-12">
                                                    <textarea id="commentText" class="inputbox" name="comment" cols="45" rows="8" aria-required="true" onfocus='if(this.value=="Comment...") this.value=""' onblur='if(this.value=="") this.value="Comment..."'>Comment...</textarea>
                                                </div>

                                            </div>

                                            <span class="clear"></span>
                                            <p class="form-submit">
                                            <input name="sbtn" type="submit" id="submitCommentButton" class="submit btn-full submit-comment" value="SUBMIT COMMENT" style="background-color:#0099cc;color:white">
                                            <input type="hidden" name="comment_post_ID" value="144" id="comment_post_ID">
                                            <input type="hidden" name="comment_parent" id="comment_parent" value="0">
                                        </p>
                                        </form>
                                    </div><!-- #respond -->

                                </div>
                            </div>
                        </div>
                        
                        <div class="the-content-sheet">

                    <div class="featured-media-con social-block sc-social-block">

                        <div class="featured-media--image divimage" style="background-image: url(img/secondary_content/social.jpg);background-color:#0099cc"></div>
                        <div class="semi-black-overlay opaque"></div>
                        <div class="row">

                            <div class="col-md-3">
                                <a href="#" class="social-meta-con">

                                    <h4>LIKE US ON</h4>
                                    <span class="social-circle-con"><i class="fa fa-facebook-square"></i></span>
                                    <div class="clear"></div>

                                    <hr>
                                    <h3>Facebook</h3>

                                </a>
                            </div>

                            <div class="col-md-3">
                                <a href="#" class="social-meta-con">

                                    <h4>FOLLOW US ON</h4>
                                    <span class="social-circle-con"><i class="fa fa-twitter"></i></span>
                                    <div class="clear"></div>

                                    <hr>
                                    <h3>Twitter</h3>

                                </a>
                            </div>

                            <div class="col-md-3">
                                <a href="#" class="social-meta-con">

                                    <h4>PIN THIS ON</h4>
                                    <span class="social-circle-con"><i class="fa fa-pinterest"></i></span>
                                    <div class="clear"></div>

                                    <hr>
                                    <h3>Pinterest</h3>

                                </a>
                            </div>

                            <div class="col-md-3">
                                <a href="#" class="social-meta-con">

                                    <h4>APPRECIATE US ON</h4>
                                    <span class="social-circle-con"><i class="fa fa-google-plus"></i></span>
                                    <div class="clear"></div>

                                    <hr>
                                    <h3>Google+</h3>

                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                        
                        
                        
            </div>
        </div>



    </div>










    <!-- this block is the navigation -->
    <nav class="qcreative--nav-con">
        <div class="translucent-con " style="">
            <div class="translucent-bg for-parallaxer"></div>
            <canvas id="tr" class="translucent-canvas"></canvas>
            <div class="translucent-overlay"></div>
        </div>

        <!-- modify logo from here -->
        <div class="logo-con">
            <div class="the-logo" style="width: 183px; background-image:url(img/logo3.png);"></div>
        </div>
           
        
        <!-- this block will be populated by gallery thumbnail navigation -->
        <ul id="menu" class="the-actual-nav">
            <li class="menu-item"><a href="index.php">Home</a></li>
            <li class="menu-item"><a href="about.php">About Us</a></li>
            
            <li class="menu-item"><a href="contact.php">contact us</a></li>
             <li class="menu-item active"><a href="msg.php">words of wisdom</a></li>
             <li class="menu-item"><a href="songs.php">Songs</a></li>
            <li class="menu-item"><a href="gallery.php">gallery</a></li>
            <li class="menu-item"><a href="vids.php">video</a></li>
                    
            
            
            
            <li id="don" class="menu-item"><a href="donate.php"><img src="img/donate.png" width="150px"></a></li>
            
           
        </ul>
        <div class="nav-social-con">
            <p class="social-icons">
                <a href="https://www.facebook.com/solomojere"><i class="fa fa-facebook-square"></i></a>
                <a href="https://www.youtube.com/channel/UC4W3y9-fbPINR7FG8LINPoQ" ><i class="fa fa-youtube"></i></a>
                <a href="https://www.pinterest.com/solomonjere/"><i class="fa fa-pinterest"></i></a>
                <a href="https://twitter.com/drsolomonmusic"><i class="fa fa-twitter"></i></a>
                <a href="https://www.instagram.com/solomonjeremusic"><i class="fa fa-instagram"></i></a>
                <!--<a href="#"><i class="fa fa-linkedin-square"></i></a>-->
            </p>
            <p class="copyright-text">COPYRIGHT &copy; Solomon Jere</p>
        </div>

    </nav>
<!-- end navigation -->



    <!-- preloader area -->
    <div class="preloader-con">
        <!--<i class="fa fa-spin fa-spinner"></i>-->
        <div class="cube-preloader"></div>
    </div>





</div>


<link rel='stylesheet' property='stylesheet' type="text/css" href="dzsselector/dzsselector.css"/>
<script src="dzsselector/dzsselector.js" type="text/javascript"></script>

<script src="qcreative/qcreative.js"></script>
<link href='advancedscroller/plugin.css' rel='stylesheet' property='stylesheet' type='text/css'>
<script src="advancedscroller/plugin.js"></script>
<link href='fontawesome/font-awesome.min.css' rel='stylesheet' property='stylesheet' type='text/css'>
<link rel='stylesheet' property='stylesheet' type="text/css" href="zoombox/zoombox.css"/>
<script src="zoombox/zoombox.js" type="text/javascript"></script>
<script src="zfolio/zfolio.js" type="text/javascript"></script>
<script src="zfolio/jquery.isotope.min.js" type="text/javascript"></script>
<script src="dzsparallaxer/dzsparallaxer.js" type="text/javascript"></script>
<script src="dzstooltip/dzstooltip.js" type="text/javascript"></script>
<script src="dzsscroller/scroller.js" type="text/javascript"></script>
<link rel='stylesheet' property='stylesheet' type="text/css" href="dzsscroller/scroller.css"/>
<!-- customizer scripts --><!-- customizer scripts END --></body>
</html>