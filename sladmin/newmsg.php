<?php
session_start();
include_once("../includes/dbconfig.php");
if(!$_SESSION['token']){
    header("Location:index.php?mode=loginerror");
}


        if(isset($_POST['sbtn'])){
            $title = $_POST['title'];
            $content = $_POST['content'];
            $pub = $_POST['pub'];
            
            $accepted = array('image/jpg','image/png','image/jpeg','image/gif');
            
            $filename = $_FILES['img']['name'];
            $type = $_FILES['img']['type'];
            $dir = "../../solo/img/";
            $temdir = $_FILES['img']['tmp_name'];
            $rename = $filename.'_'.time();
            
                if(in_array($type,$accepted)){
                    $upld = move_uploaded_file($temdir,$dir.$rename);
                        if($upld){
                           $qry = "insert into msg values($title,$content,$pub)"; 
                            $rslt = mysql_query($qry);
                                if($rslt){
                                    header("Location:dash.php?mode=new");
                                }
                        }
                    
                }else{
                    echo "Wrong file type. Allowed file types are png,jpeg, jpg only";
                }
            
            
                
            
            
            
        }

?>


<!DOCTYPE html>
<html lang="en">
    
    
<script src="tinymce/js/tinymce/tinymce.min.js"></script>
    
    <script type="text/javascript">
    tinymce.init({
            selector: '#mytextarea'
        });
  </script>
    
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SL Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">SL Admin</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
               
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href=""><i class="fa fa-envelope fa-fw fa-2x"></i>Messages<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="dash.php">All messages</a>
                                </li>
                                <li>
                                    <a href="pub.php">published messages</a>
                                </li>
                                <li>
                                    <a href="unpub.php">yet to be published</a>
                                </li>
                                <li>
                                    <a href="newmsg.php">New Message</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="upldpics.php"><i class="fa fa-file-image-o fa-2x"></i> upload gallery Images</a>
                        </li>
                        <li>
                            <a href="upldvid.php"><i class="fa fa-video-camera  fa-2x"></i> Upload Videos</a>
                        </li>
                        
                        
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">New Message</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        New Message
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6" style="width:100%">
                                    
                                    <form role="form" method="post" action="" enctype="multipart/form-data">
                                        
                                        <div class="form-group">
                                            <label>Title</label>
                                            <input class="form-control" type="text" name="title" required>
                                        </div>
                                        
                                        
                                        <div class="form-group">
                                            <label>Message</label>
                                            <textarea class="form-control" rows="10" cols="10" name="content" id="mytextarea"></textarea>
                                        </div>
                                        
                                        
                                        <div class="form-group">
                                            <label>publish now</label>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="pub"  value="yes" checked>YES
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="pub"  value="no">NO
                                                </label>
                                            </div>
                                            
                                        </div>
                                        <div class="form-group">
                                            <label>Upload Image</label>
                                            <input type="file" name="img" required> 
                                        </div>
                                        
                                        <button type="submit" class="btn btn-primary" name="sbtn">Submit Button</button>
                                        <button type="reset" class="btn btn-default" onclick="history.go(-1)">Reset Button</button>
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
