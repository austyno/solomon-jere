<?php
session_start();
include_once("../includes/dbconfig.php");
if(!$_SESSION['token']){
    header("Location:index.php?mode=loginerror");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="refresh" content="url=dash.php">

    <title>SL Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    
    
    <style>
    button.accordion {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 18px;
    width: auto;
    text-align: left;
    border: none;
    outline: none;
    transition: 0.4s;
}
    button.accordion.active, button.accordion:hover {
    background-color: #ddd;
}
#panel {
    padding: 0 18px;
    background-color: white;
    display: none;
    transition: max-height 0.2s ease-out;
}
    
button.accordion:after {
    content: '\02795';
    font-size: 13px;
    color: #777;
    float: right;
    margin-left: 5px;
}

button.accordion.active:after {
    content: "\2796";
}

    </style>
    
    
    
       
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">SL Admin</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
               
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="#"><i class="fa fa-envelope fa-fw fa-2x"></i>Messages<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="dash.php">All messages</a>
                                </li>
                                <li>
                                    <a href="pub.php">Published messages</a>
                                </li>
                                <li>
                                    <a href="unpub.php">Yet to be published</a>
                                </li>
                                <li>
                                    <a href="newmsg.php">New Message</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="upldpics.php"><i class="fa fa-file-image-o fa-2x"></i> upload gallery Images</a>
                        </li>
                        <li>
                            <a href="upldvids.php"><i class="fa fa-video-camera  fa-2x"></i> Upload Videos</a>
                        </li>
                        
                        <li>
                            <a href="songs.php"><i class="fa fa-music fa-2x"></i> songs</a>
                              
                        </li>
                        
                        
                        
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    
                    <?php 
                    if(isset($_GET['mode'])){
$mode = $_GET['mode'];
if($mode == 'success'){
    echo'<div class="alert alert-success alert-dismissable" style="width:500px;margin:auto">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            The message has been updated successfuly.
    </div>';
    }elseif($mode == 'del'){
            $id = $_GET['id'];
            echo '<div class="alert alert-danger" style="width:auto;float:right;margin-top:32px;margin-buttom:0px" >
                    Are you sure you want to <strong>Delete This Message</strong>.<a href="dash.php?del=yes&id='.$id.'" class="alert-link btn btn-default btn-xs">YES</a> <a href="dash.php" class="alert-link btn btn-default btn-xs">NO</a>.
            </div>';
    
    }elseif($mode == 'new'){
        echo'<div class="alert alert-success alert-dismissable" style="width:500px;margin:auto">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            The message has been created successfuly.
    </div>';
    }elseif($mode == 'saved'){
         echo '<div class="alert alert-success alert-dismissable" style="width:auto;float:right;margin-top:32px;margin-buttom:0px"> 
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                   The new Song has been successfully uploaded
                </div>';
    
    }
}
                    
                    
                    
                    
if(isset($_GET['del'])){
    $del = $_GET['del'];
    $id = $_GET['id'];
    
    $qry = "delete from msg where msg_id = $id";
    $rslt = mysql_query($qry);
        if($rslt){
                
               echo '<div class="alert alert-success alert-dismissable" style="width:auto;float:right;margin-top:32px;margin-buttom:0px"> 
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Message has been deleted successfully
                            </div>';
        }
    
}
                    
?>                    
                    
                    <div>
                    <h1 class="page-header">Songs</h1>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            All Songs 
                        </div>
                        
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>S/N</th>
                                        <th>Song Title</th> 
                                        <th>Song</th>
                                        <th>Song Status</th>
                                        <th>Link</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                            $qry = "select * from songs";
                            $rslt = mysql_query($qry);
                            $rs = mysql_fetch_assoc($rslt);
                            $found = mysql_num_rows($rslt);
                                if($found > 0){
                                    
                                    $cnt = 1;
                                    
                                    do{
                                        
                                   echo '<tr class="odd gradeC">
                                        <td>'.$cnt.'</td>
                                        <td>'.$rs['sng_title'].'</td>
                                        <td>'.$rs['sng_song'].'</td>
                                        <td>'.$rs['sng_status'].'</td>
                                        <td>'.$rs['sng_link'].'</td>
                                        
                                        
                                        
                                    </tr>'; 
                                        
                                        $cnt++;
                                    }while($rs = mysql_fetch_assoc($rslt));
                                    
                                }
                            
                        ?>
                                    
                                </tbody>
                            </table>
                            
                            
                            
                            <?php 
                                
                                if(isset($_POST['sbtn'])){
                                    
                                    $title = $_POST['title'];
                                    $status = $_POST['status'];
                                    $link = $_POST['link'];
                                    $filename = $_FILES['sng']['name'];
                                    $dir = "../songs/";
                                    $temp = $_FILES['sng']['tmp_name'];
                                    $type = $_FILES['sng']['type'];
                                    $allowed = array();
                                    
                                    $upld = move_uploaded_file($temp,$dir.$filename);
                                        if($upld){
                                           $sql = "insert into songs (sng_id,sng_title,sng_song,sng_status,sng_link)values(null,'$title','$filename','$status','$link')";
                                            $rslt = mysql_query($sql);
                                                if($rslt){
                                                   echo'
                                                   <script>';
                                                    window.location.href = "songs.php?mode=saved"
                                                   echo'</script>';
                                                   ';
                                                    
                                                }
                                        }
                                    
                                }
                            
                            
                            
                            
                            
                            ?>
                            
                            <button class="accordion">Upload New Song</button>
                            <div class="well" id="panel">
                                <h4>Upload New song</h4>
                                <div class="col-lg-12">
                    <div class="panel panel-default" style="background-color:">
                        <div class="panel-heading">
                            New Song
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6"  style="width:100%">
                                    <form role="form" method="post" action="" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label>Song Title</label>
                                            <input type="text" name="title" class="form-control" placeholder="Enter Song Title...">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Song Itunes Link</label>
                                            <input type="text" name="link" class="form-control" placeholder="Enter Song Itune Link...">
                                        </div>
                                        
                                        
                                        <div class="form-group">
                                            <label>Upload Song File</label>
                                            <input type="file" name="sng">
                                        </div>
                                        
            
                                        <div class="form-group">
                                            <label>Song Status :</label>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="status" value="free" checked>FREE
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="status" value="buy">BUY
                                                </label>
                                            </div>
                                            
                                        </div>
                                        <button type="submit" name="sbtn" class="btn btn-default">Submit Button</button>
                                        <button type="reset" class="btn btn-default" onclick="history.go(-1)">Reset Button</button>
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <!-- /.row -->
            
            <!-- /.row -->
            
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
        
        
   var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");

        var panel = document.getElementById("panel")
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    }
}
        
        
        
        
    </script>

</body>

</html>
