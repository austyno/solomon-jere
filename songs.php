<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="img/icon2.ico" />



    <!-- - - main style scripts -->

    <link rel='stylesheet' property='stylesheet' type="text/css" href="./bootstrap/bootstrap.min.css"/>
    <link rel='stylesheet' property='stylesheet' type="text/css" href="./qcreative/qcreative.css"/>
    <link rel='stylesheet' property='stylesheet' type="text/css" href="./qcreative/include_et.css"/>
    <link rel='stylesheet' property='stylesheet' type="text/css" href="dzsparallaxer/dzsparallaxer.css"/>
    <script src="js/jquery.js"></script>

    <link rel='stylesheet' property='stylesheet' type="text/css" href="dzstooltip/dzstooltip.css"/>

    <!-- font inclusions 
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:700italic,400,400italic,600,600italic,700,800' rel='stylesheet' property='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,400italic,700,900,900italic' rel='stylesheet' property='stylesheet' type='text/css'>-->
    <!-- font inclusions END-->


    <link rel='stylesheet' property='stylesheet' type="text/css" href="zfolio/zfolio.css"/>


    <script class="mainoptions">

        // -- page options come here


        window.qcreative_options = {
            images_arr: ['img/backgrounds/21.jpg']  // -- the background
            ,enable_ajax: 'off' // -- enable "on" - ajax transition between pages / disable "off"
            ,bg_isparallax: 'on' // -- apply a parallax effect on scroll


        };
        // -- page options END here
    </script>
    <!-- preseter styles -->
    <link rel='stylesheet' property='stylesheet' type="text/css" href="dzstooltip/dzstooltip.css"/>

    <!-- preseter styles END-->
    
    
    <style>
        #menu li a:hover{
            background-color:#0099cc;
        }
        
        #menu li ul li a{
             background-color:#0099cc;
        }
        #menu li ul li:hover > a{
            background-color:#0099cc;
        }
        .active a{
            background-color: #0099cc; 
        }
        #don{
            font-style: oblique;
            
        }
        #don a{
            color: #0099cc;
        
        }
    </style>
    
    
    
    
</head>
<body class="page-normal post-type-page content-align-center page-title-align-right page-title-style-2 menu-type-1">



<!-- this is where the main background will come, set image from above ( images_arr ) -->
<div class="main-bg-con dzsparallaxer " data-options='{  mode_scroll: "fromtop" }'>
    <figure class="main-bg dzsparallaxer--target" style=""></figure>
</div>


<div class="main-container">




    <div class="the-content-con">

        <!-- page title -->
        <h1 class="" style="color:#0099cc"></h1>


        <div class="the-content">

            <!-- markup for blur // do not modify -->
            <div class="translucent-con ">
                <div class="translucent-bg  for-parallaxer"></div>
                <canvas class="translucent-canvas"></canvas>
                <div class="translucent-overlay"></div>
            </div>


            <div class="the-content-inner">
                <!-- section start -->
                <div class="the-content-sheet">

                    <!-- section start -->
                    <div class="the-content-sheet-text">

                        <!-- section title-->
                        <div class="row row-margin">
                            <div class="col-md-12">
                                <h2><br></h2>
                            </div>
                        </div>
                        <div class="section-number"></div>

                        <!-- some dividers that can be used <> markups -->

                        <div class="row row-margin ">
                            <div class="col-md-12">


                                <div id="ag1" class="audiogallery skin-bluelights" style="">
                                    <div class="items">
                                        
                                        <?php
                                        
                                        include_once("includes/dbconfig.php");
                                        
                                        $qry = "select * from songs";
                                        $rslt = mysql_query($qry);
                                        $rs = mysql_fetch_assoc($rslt);
                                        
                                        
                                            do{
                                                
                                            echo '<div  class="audioplayer-tobe " style=" " data-type="audio" data-source="songs/'.$rs['sng_song'].'">
                                            <div class="meta-artist">
                                                <span class="the-artist">Solomon Jere</span>
                                                <span class="the-artist">'.$rs['sng_title'].'</span>
                                            </div>

                                            <div class="extra-html-in-controls-right">';
                                                
                                                    if($rs['sng_status'] == 'buy'){
                                                       echo '<a class="btn-zoomsounds" href="'.$rs['sng_link'].'" target="_blank" >BUY</a>';
                                                    }else{
                                                echo '<a class="btn-zoomsounds" href="download.php?songid='.$rs['sng_id'].'">DOWNLOAD</a>';
                                                    }
                                                
                                           echo '</div>
                                        </div>';
                                                
                                                
                                            }while($rs = mysql_fetch_assoc($rslt));
                                        
                                        
                                        
                                        
                                        ?>
                                        
                                    </div>

                                    <!-- audio playlist items markup END -->
                                </div>
                                <!-- audio playlist  markup END -->

                            </div>
                        </div>

                    </div>
                    
                </div>
                
            <div class="the-content-sheet">

                    <div class="featured-media-con social-block sc-social-block">

                        <div class="featured-media--image divimage" style="background-image: url(img/secondary_content/social.jpg);background-color:#0099cc"></div>
                        <div class="semi-black-overlay opaque"></div>
                        <div class="row">

                            <div class="col-md-3">
                                <a href="#" class="social-meta-con">

                                    <h4>LIKE US ON</h4>
                                    <span class="social-circle-con"><i class="fa fa-facebook-square"></i></span>
                                    <div class="clear"></div>

                                    <hr>
                                    <h3>Facebook</h3>

                                </a>
                            </div>

                            <div class="col-md-3">
                                <a href="#" class="social-meta-con">

                                    <h4>FOLLOW US ON</h4>
                                    <span class="social-circle-con"><i class="fa fa-twitter"></i></span>
                                    <div class="clear"></div>

                                    <hr>
                                    <h3>Twitter</h3>

                                </a>
                            </div>

                            <div class="col-md-3">
                                <a href="#" class="social-meta-con">

                                    <h4>PIN THIS ON</h4>
                                    <span class="social-circle-con"><i class="fa fa-pinterest"></i></span>
                                    <div class="clear"></div>

                                    <hr>
                                    <h3>Pinterest</h3>

                                </a>
                            </div>

                            <div class="col-md-3">
                                <a href="#" class="social-meta-con">

                                    <h4>APPRECIATE US ON</h4>
                                    <span class="social-circle-con"><i class="fa fa-google-plus"></i></span>
                                    <div class="clear"></div>

                                    <hr>
                                    <h3>Google+</h3>

                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                
                
                
                
                
                
                
                
            </div>    
        </div>
        
    </div>
    
    
    
    
    










    <!-- this block is the navigation -->
    <nav class="qcreative--nav-con">
        <div class="translucent-con " style="">
            <div class="translucent-bg for-parallaxer"></div>
            <canvas id="tr" class="translucent-canvas"></canvas>
            <div class="translucent-overlay"></div>
        </div>

        <!-- modify logo from here -->
        <div class="logo-con">
            <div class="the-logo" style="width: 183px; background-image:url(img/logo3.png);"></div>
        </div>
            
        
        <!-- this block will be populated by gallery thumbnail navigation -->
        <ul id="menu" class="the-actual-nav">
            <li class="menu-item"><a href="index.php">Home</a></li>
            <li class="menu-item"><a href="about.php">About Us</a></li>
            
            <li class="menu-item"><a href="contact.php">contact us</a></li>
             <li class="menu-item"><a href="msg.php">words of wisdom</a></li>
             <li class="menu-item active"><a href="songs.php">Songs</a></li>
            <li class="menu-item"><a href="gallery.php">gallery</a></li>
            <li class="menu-item"><a href="vids.php">video</a></li>
                    
            
            
            
            <li id="don" class="menu-item"><a href="donate.php"><img src="img/donate.png" width="150px"></a></li>
            
           
        </ul>
        <div class="nav-social-con">
            <p class="social-icons">
                <a href="https://www.facebook.com/solomojere"><i class="fa fa-facebook-square"></i></a>
                <a href="https://www.youtube.com/channel/UC4W3y9-fbPINR7FG8LINPoQ" ><i class="fa fa-youtube"></i></a>
                <a href="https://www.pinterest.com/solomonjere/"><i class="fa fa-pinterest"></i></a>
                <a href="https://twitter.com/drsolomonmusic"><i class="fa fa-twitter"></i></a>
                <a href="https://www.instagram.com/solomonjeremusic"><i class="fa fa-instagram"></i></a>
                <!--<a href="#"><i class="fa fa-linkedin-square"></i></a>-->
            </p>
            <p class="copyright-text">COPYRIGHT &copy; Solomon Jere</p>
        </div>

    </nav>
<!-- end navigation -->



    <!-- preloader area -->
    <div class="preloader-con">
        <!--<i class="fa fa-spin fa-spinner"></i>-->
        <div class="cube-preloader"></div>
    </div>





</div>


<!-- lightbox settings -->
<script class="zoombox-settings">



    window.init_zoombox_settings = {
        settings_zoom_doNotGoBeyond1X:'off'
        ,design_skin:'skin-darkfull'
        ,settings_enableSwipe:'on'
        ,settings_enableSwipeOnDesktop:'on'
        ,settings_galleryMenu:'dock'
        ,settings_useImageTag:'on'
        ,settings_paddingHorizontal : '100'
        ,settings_paddingVertical : '100'
        ,settings_disablezoom:'on'
        ,settings_transition : 'fade'
        ,settings_transition_out : 'fade'
        ,videoplayer_settings: {
            design_skin: 'skin_reborn'
            ,zoombox_video_autoplay: "off"
            ,settings_youtube_usecustomskin: "off"
            ,settings_video_overlay: "on"
        }
    };
</script>
<!-- lightbox settings  END -->


<!--
- - next will come the loaded scripts
-->


<link rel='stylesheet' property='stylesheet' type="text/css" href="dzsselector/dzsselector.css"/>
<script src="dzsselector/dzsselector.js" type="text/javascript"></script>

<script src="qcreative/qcreative.js"></script>
<link href='advancedscroller/plugin.css' rel='stylesheet' property='stylesheet' type='text/css'>
<script src="advancedscroller/plugin.js"></script>
<link href='fontawesome/font-awesome.min.css' rel='stylesheet' property='stylesheet' type='text/css'>
<link rel='stylesheet' property='stylesheet' type="text/css" href="zoombox/zoombox.css"/>
<script src="zoombox/zoombox.js" type="text/javascript"></script>
<script src="zfolio/zfolio.js" type="text/javascript"></script>
<script src="zfolio/jquery.isotope.min.js" type="text/javascript"></script>
<script src="dzsparallaxer/dzsparallaxer.js" type="text/javascript"></script>
<script src="dzstooltip/dzstooltip.js" type="text/javascript"></script>
<link rel='stylesheet' property='stylesheet' type="text/css" href="dzsprogressbars/dzsprogressbars.css"/>
<script src="dzsprogressbars/dzsprogressbars.js" type="text/javascript"></script>
dddddd<link rel='stylesheet' property='stylesheet' type="text/css" href="dzstabsandaccordions/dzstabsandaccordions.css"/>
dddddd<script src="dzstabsandaccordions/dzstabsandaccordions.js"></script>
<script src="dzsscroller/scroller.js" type="text/javascript"></script>
<link rel='stylesheet' property='stylesheet' type="text/css" href="dzsscroller/scroller.css"/>

<link rel='stylesheet' property='stylesheet' type="text/css" href="videogallery/vplayer.css"/>
<script type="text/javascript" src="videogallery/vplayer.js"></script>
<link rel='stylesheet' type="text/css" href="audioplayer/audioplayer.css"/>
<script src="audioplayer/audioplayer.js" type="text/javascript"></script>

<!-- customizer scripts --><!-- customizer scripts END -->
<!-- customizer scripts --><!-- customizer scripts END --></body>
</html>