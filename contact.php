<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel='stylesheet' property='stylesheet' type="text/css" href="./bootstrap/bootstrap.min.css"/>
    <link rel='stylesheet' property='stylesheet' type="text/css" href="./qcreative/qcreative.css"/>
    <link rel='stylesheet' property='stylesheet' type="text/css" href="./qcreative/include_et.css"/>

    <link rel="icon" href="img/icon2.ico" />
    <link rel='stylesheet' property='stylesheet' type="text/css" href="zfolio/zfolio.css"/>

    <link rel='stylesheet' property='stylesheet' type="text/css" href="dzsparallaxer/dzsparallaxer.css"/>
    <script src="js/jquery.js"></script>




    <!-- font inclusions 
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:700italic,400,400italic,600,600italic,700,800' rel='stylesheet' property='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,400italic,700,900,900italic' rel='stylesheet' property='stylesheet' type='text/css'>-->
    <!-- font inclusions END-->


    <link rel='stylesheet' property='stylesheet' type="text/css" href="dzstooltip/dzstooltip.css"/>
    <script class="mainoptions">

        // -- page options come here


        window.qcreative_options = {
            images_arr: ['img/backgrounds/21.jpg']  // -- the background
            ,enable_ajax: 'off' // -- enable "on" - ajax transition between pages / disable "off"
            ,bg_isparallax: 'on' // -- apply a parallax effect on scroll

            ,content_width: "0" // -- set a custom content width ( in px ), leave "0" for default css content width

//            -- page options END here
        };
    </script>
    <!-- preseter styles -->
    <link rel='stylesheet' property='stylesheet' type="text/css" href="dzstooltip/dzstooltip.css"/>

    <!-- preseter styles END-->
    
     <style>
        #tr{
          
            background-color:black;
        
        }
        #menu li a:hover{
            background-color:#0099cc;
        }
        
        #menu li ul li a{
             background-color:#0099cc;
        }
        #menu li ul li:hover > a{
            background-color:#0099cc;
        }
        .active a{
            background-color:#0099cc;
            
        }
         
         .formbtn{
             background-color: lightskyblue;
         }
         .formbtn:hover{
             background-color:#0099cc;
         }
         #don{
            font-style: oblique;
            
        }
        #don a{
            color:#0099cc;
        }
    </style>
    
    
    
    
</head>
<body class="page-contact post-type-page content-align-center page-title-align-right page-title-style-2 menu-type-1">

<div class="main-bg-con dzsparallaxer " data-options='{  mode_scroll: "fromtop" }'>

    <figure class="main-bg dzsparallaxer--target" style=""></figure>
</div>


<div class="main-container">




    <div class="the-content-con">




        <h1 class="" style="color:#0099cc;">
        
        </h1>


        <div class="the-content">

            <div class="translucent-con ">
                <div class="translucent-bg  for-parallaxer"></div>
                <canvas class="translucent-canvas"></canvas>
                <div class="translucent-overlay"></div>
            </div>



            <div class="the-content-inner">

                <div class="the-content-sheet">

                    <div class="featured-media-con contact-featured-media-con">

                        <div class="featured-media--image divimage" style="background-image: url(img/contact/street.jpg);"></div>

                        <div class="contact-info">
                            <h4>HEADQUARTERS</h4>
                            <hr class="qcreative-hr-small">
                            <p>Lilayi Estate<br>
                                P O Box 340025 Libala <br>
                                South Lusaka Zambia
                                </p>
                            <h4>PHONE / FAX</h4>
                            <hr class="qcreative-hr-small">
                            <p>+260963014839<br>
                                +2349087841925</p>
                            <h4>EMAIL</h4>
                            <hr class="qcreative-hr-small">
                            <p><a href="#">info@solomonjere.com</a></p>
                            <hr>
                            <a href="#"  class="social-circle-con"><i class="fa fa-facebook-square"></i></a>
                            <a href="#"  class="social-circle-con"><i class="fa fa-behance"></i></a>
                            <a href="#" class="social-circle-con"><i class="fa fa-pinterest-square"></i></a>
                            <a href="#" class="social-circle-con"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="social-circle-con"><i class="fa fa-linkedin-square"></i></a>


                        </div>
                        <div class="view-map-overflower">
                            <div class="map-toggler map-show"><span class="">VIEW MAP</span></div>
                        </div>
                    </div>


                </div>
                
                
                    <?php
                    
                        if(isset($_POST['the_name'])){
                            $name =$_POST['the_name'];
                            $email =$_POST['the_email'];
                            $feedback =$_POST['the_feedback'];




                            $to = 'info@solomonjere.com';
                            $subject = 'Received Feedback';
                            $message = '';
                            $message.= '<p><strong>Name:</strong></p><p>'.$name.'</p>';
                            $message.= '<p><strong>Email:</strong></p><p>'.$email.'</p>';
                            $message.= '<p><strong>Feedback:</strong></p><p>'.$feedback.'</p>';
                            $headers  = 'MIME-Version: 1.0' . "\r\n";
                            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                            $headers.= 'From: '. $email . "\r\n" .
                                'Reply-To: '. $email  . "\r\n" .
                                'X-Mailer: PHP/' . phpversion();
                           $mail =  mail($to, $subject, $message, $headers);

                            if($mail){
                                echo'
                                        <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                We have recieved your mail.Thank You for contacting us</strong>.
                            </div>
                                    ';
                            }
                        }



                
                ?>




                <div class="the-content-sheet the-content-sheet-for-form formbtn">

                    <div class="featured-media-con" style=" height: 100%">

                        <div class="featured-media--image divimage"></div>
                    </div>

                    <form action="" class="form-overlay contact-form">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="social-meta-con">

                                    <h4>SEND US A</h4>
                                    <span class="social-circle-con"><i class="fa fa-envelope"></i></span>
                                    <div class="clear"></div>

                                    <hr>
                                    <h3>Message</h3>

                                </div>
                            </div>
                            <button class="btn-full-white contact-form-button formbtn" type="submit">SEND MESSAGE</button>
                            <div class="col-md-9">
                                <div class="row smaller-padding">
                                    <div class="col-md-6">

                                        <input type="text" name="the_name" class="input-for-name" placeholder="Name..."/>
                                    </div>
                                    <div class="col-md-6">

                                        <input type="text" name="the_email" class="input-for-email" placeholder="Email..."/>
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <textarea name="the_feedback" class="input-for-feedback" placeholder="Feedback..."></textarea>

                                <div class="form-feedback">THANK YOU, YOUR MESSAGE HAS BEEN SENT</div>
                            </div>
                        </div>
                    </form>

                </div>



<div class="the-content-sheet">

                    <div class="featured-media-con social-block sc-social-block">

                        <div class="featured-media--image divimage" style="background-image: url(img/secondary_content/social.jpg);background-color:#0099cc"></div>
                        <div class="semi-black-overlay opaque"></div>
                        <div class="row">

                            <div class="col-md-3">
                                <a href="#" class="social-meta-con">

                                    <h4>LIKE US ON</h4>
                                    <span class="social-circle-con"><i class="fa fa-facebook-square"></i></span>
                                    <div class="clear"></div>

                                    <hr>
                                    <h3>Facebook</h3>

                                </a>
                            </div>

                            <div class="col-md-3">
                                <a href="#" class="social-meta-con">

                                    <h4>FOLLOW US ON</h4>
                                    <span class="social-circle-con"><i class="fa fa-twitter"></i></span>
                                    <div class="clear"></div>

                                    <hr>
                                    <h3>Twitter</h3>

                                </a>
                            </div>

                            <div class="col-md-3">
                                <a href="#" class="social-meta-con">

                                    <h4>PIN THIS ON</h4>
                                    <span class="social-circle-con"><i class="fa fa-pinterest"></i></span>
                                    <div class="clear"></div>

                                    <hr>
                                    <h3>Pinterest</h3>

                                </a>
                            </div>

                            <div class="col-md-3">
                                <a href="#" class="social-meta-con">

                                    <h4>APPRECIATE US ON</h4>
                                    <span class="social-circle-con"><i class="fa fa-google-plus"></i></span>
                                    <div class="clear"></div>

                                    <hr>
                                    <h3>Google+</h3>

                                </a>
                            </div>
                        </div>
                    </div>
                </div>














            </div>



        </div>
    </div>









    <!-- this block is the navigation -->
    <nav class="qcreative--nav-con">
        <div class="translucent-con " style="">
            <div class="translucent-bg for-parallaxer"></div>
            <canvas id="tr" class="translucent-canvas"></canvas>
            <div class="translucent-overlay"></div>
        </div>

        <!-- modify logo from here -->
        <div class="logo-con">
            <div class="the-logo" style="width: 183px; background-image:url(img/logo3.png);"></div>
        </div>
            
        
        <!-- this block will be populated by gallery thumbnail navigation -->
        <ul id="menu" class="the-actual-nav">
            <li class="menu-item"><a href="index.php">Home</a></li>
            <li class="menu-item"><a href="about.php">About Us</a></li>
            
            <li class="menu-item active"><a href="contact.php">contact us</a></li>
             <li class="menu-item"><a href="msg.php">words of wisdom</a></li>
             <li class="menu-item"><a href="songs.php">Songs</a></li>
            <li class="menu-item"><a href="gallery.php">gallery</a></li>
            <li class="menu-item"><a href="vids.php">video</a></li>
                    
            
            
            
            <li id="don" class="menu-item"><a href="donate.php"><img src="img/donate.png" width="150px"></a></li>
            
           
        </ul>
        <div class="nav-social-con">
            <p class="social-icons">
                <a href="https://www.facebook.com/solomojere"><i class="fa fa-facebook-square"></i></a>
                <a href="https://www.youtube.com/channel/UC4W3y9-fbPINR7FG8LINPoQ" ><i class="fa fa-youtube"></i></a>
                <a href="https://www.pinterest.com/solomonjere/"><i class="fa fa-pinterest"></i></a>
                <a href="https://twitter.com/drsolomonmusic"><i class="fa fa-twitter"></i></a>
                <a href="https://www.instagram.com/solomonjeremusic"><i class="fa fa-instagram"></i></a>
                <!--<a href="#"><i class="fa fa-linkedin-square"></i></a>-->
            </p>
            <p class="copyright-text">COPYRIGHT &copy; Solomon Jere</p>
        </div>

    </nav>
<!-- end navigation -->



    <!-- preloader area -->
    <div class="preloader-con">
        <i class="fa fa-spin fa-spinner"></i>
        <!--<div class="cube-preloader"></div>-->
    </div>

    <div class="map-canvas-con">

        <div id="map-canvas" class="map-canvas big-map" data-lat="-15.45" data-long="28.31667"></div>
         <div class="contact-info">
                            <h4>HEADQUARTERS</h4>
                            <hr class="qcreative-hr-small">
                            <p>Lilayi Estate<br>
                                P O Box 340025 Libala <br>
                                South Lusaka Zambia
                                </p>
                            <h4>PHONE / FAX</h4>
                            <hr class="qcreative-hr-small">
                            <p>+260963014839<br>
                                +2349087841925</p>
                            <h4>EMAIL</h4>
                            <hr class="qcreative-hr-small">
                            <p><a href="#">info@solomonjere.com</a></p>
                            <hr>
                            <a href="#"  class="social-circle-con"><i class="fa fa-facebook-square"></i></a>
                            <a href="#"  class="social-circle-con"><i class="fa fa-behance"></i></a>
                            <a href="#" class="social-circle-con"><i class="fa fa-pinterest-square"></i></a>
                            <a href="#" class="social-circle-con"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="social-circle-con"><i class="fa fa-linkedin-square"></i></a>

                            <div class="services-lightbox--close map-hide"><i class="fa fa-times"></i></div>
                        </div>

        <!--<div class="map-toggler map-hide">-->
        <!--<div class="q-close-btn ">-->

        <!--</div>-->
        <!--</div>-->
    </div>
</div>


<script>


    window.init_zoombox_settings = {
        settings_zoom_doNotGoBeyond1X:'off'
        ,design_skin:'skin-whitefull'
        ,settings_enableSwipe:'on'
        ,settings_enableSwipeOnDesktop:'on'
        ,settings_galleryMenu:'dock'
        ,settings_useImageTag:'on'
        ,settings_paddingHorizontal : '0'
        ,settings_paddingVertical : '0'
    };



</script>



<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

<!--?key=AIzaSyADxgXowMx3MB2gdNiY-Z0XkB3MpMk8xJg-->
<link rel='stylesheet' property='stylesheet' type="text/css" href="dzsselector/dzsselector.css"/>
<script src="dzsselector/dzsselector.js" type="text/javascript"></script>

<script src="qcreative/qcreative.js"></script>
<link href='advancedscroller/plugin.css' rel='stylesheet' property='stylesheet' type='text/css'>
<script src="advancedscroller/plugin.js"></script>
<link href='fontawesome/font-awesome.min.css' rel='stylesheet' property='stylesheet' type='text/css'>
<link rel='stylesheet' property='stylesheet' type="text/css" href="zoombox/zoombox.css"/>
<script src="zoombox/zoombox.js" type="text/javascript"></script>
<script src="zfolio/zfolio.js" type="text/javascript"></script>
<script src="zfolio/jquery.isotope.min.js" type="text/javascript"></script>
<script src="dzsparallaxer/dzsparallaxer.js" type="text/javascript"></script>
<script src="dzstooltip/dzstooltip.js" type="text/javascript"></script>
<link rel='stylesheet' property='stylesheet' type="text/css" href="dzsprogressbars/dzsprogressbars.css"/>
<script src="dzsprogressbars/dzsprogressbars.js" type="text/javascript"></script>
<script src="dzsscroller/scroller.js" type="text/javascript"></script>
<link rel='stylesheet' property='stylesheet' type="text/css" href="dzsscroller/scroller.css"/>
<!-- customizer scripts --><!-- customizer scripts END --></body>
</html>