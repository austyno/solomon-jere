<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="img/icon2.ico" />


    <!-- - - main style scripts -->
    <link rel='stylesheet' property='stylesheet' type="text/css" href="./bootstrap/bootstrap.min.css"/>
    <link rel='stylesheet' property='stylesheet' type="text/css" href="./qcreative/qcreative.css"/>
    <link rel='stylesheet' property='stylesheet' type="text/css" href="dzsparallaxer/dzsparallaxer.css"/>
    <script src="js/jquery.js"></script>

    <!-- font inclusions 
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:700italic,400,400italic,600,600italic,700,800' rel='stylesheet' property='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,400italic,700,900,900italic' rel='stylesheet' property='stylesheet' type='text/css'>-->
    <!-- font inclusions END-->


    <link rel='stylesheet' property='stylesheet' type="text/css" href="zfolio/zfolio.css"/>


    <script class="mainoptions">

        // -- page options come here


        window.qcreative_options = {
            images_arr: ['img/backgrounds/21.jpg'] // -- the background
            ,enable_ajax: 'off' // -- enable "on" - ajax transition between pages / disable "off"
            ,bg_isparallax: 'on' // -- apply a parallax effect on scroll
            ,gallery_w_thumbs_autoplay_videos: 'off' // -- autoplay videos on video item select


//            -- page options END here
        };
    </script>
    <!-- preseter styles -->
    <link rel='stylesheet' property='stylesheet' type="text/css" href="dzstooltip/dzstooltip.css"/>

    <!-- preseter styles END-->
    
    
    <style>
        #menu li a:hover{
            background-color:#0099cc;
        }
        
        #menu li ul li a{
             background-color:#0099cc;
        }
        #menu li ul li:hover > a{
            background-color:#0099cc;
        }
        .active a{
            background-color: #0099cc;
            
        }
        #don{
            font-style: oblique;
            
        }
        #don a{
            color:#0099cc;
        }
    </style>  
</head>
<body class="page-gallery-w-thumbs post-type-page content-align-center page-title-align-right page-title-style-1 menu-type-1">


<!-- markup for background // do not modify -->
<div class="main-bg-con" data-options='{  mode_scroll: "fromtop" }'>
    <figure class="main-bg " style=""></figure>
</div>



<div class="main-container">





    <div class="the-content-con ">


        <!-- the page title -->
        <h1 class="">GALLERY</h1>

        <div class="the-content gallery-thumbs--image-container">

            <!-- markup for blur // do not modify -->
            <div class="translucent-con ">
                <div class="translucent-bg  for-parallaxer"></div>
                <canvas class="translucent-canvas"></canvas>
                <div class="translucent-overlay"></div>
            </div>
            <div class="the-content-inner">




                <!-- this block will be populated by gallery items-->

                <div id="as-gallery-w-thumbs" class="advancedscroller skin-karma-inset auto-init-from-q" style="width:100%;"><div class="preloader"></div>
                    <ul class="items">
                        
                        <?php
                        
                          include_once("includes/dbconfig.php");
                            $qry = "select * from vids";
                            $rslt = mysql_query($qry);
                            $rs = mysql_fetch_assoc($rslt);
                        
                                do{
                                    
                           echo '<li class="item-tobe  " data-source="'.$rs['vid_video'].'" data-gallery-thumbnail="img/gallery_w_thumbs/thumbs/'.$rs['vid_thumbnail'].'" data-type="video" data-width-for-gallery="898" data-height-for-gallery="506">
                            
                        </li>';
                                    
                                    
                                    
                                    
                                    
                                }while($rs = mysql_fetch_assoc($rslt));
                            
                        ?>

                    </ul>
                </div>


            </div>


        </div>
        
        <!-- this block will be AUTO populated by gallery thumbnail navigation // do not modify -->
        <div class="gallery-thumbs-con">

            <div class="translucent-con ">
                <div class="translucent-bg  for-parallaxer"></div>
                <canvas class="translucent-canvas"></canvas>
                <div class="translucent-overlay"></div>
            </div>
            <div class="thumbs-list-con">

                <ul class="thumbs-list">

                </ul>
            </div>
        </div>

        <div class="the-content-bg-placeholder"></div>




    </div>



    <!-- this block is the navigation -->
    <nav class="qcreative--nav-con">
        <div class="translucent-con " style="">
            <div class="translucent-bg for-parallaxer"></div>
            <canvas id="tr" class="translucent-canvas"></canvas>
            <div class="translucent-overlay"></div>
        </div>

        <!-- modify logo from here -->
        <div class="logo-con">
            <div class="the-logo" style="width: 183px; background-image:url(img/logo3.png);"></div>
        </div>
            
        
        <!-- this block will be populated by gallery thumbnail navigation -->
        <ul id="menu" class="the-actual-nav">
            <li class="menu-item"><a href="index.php">Home</a></li>
            <li class="menu-item"><a href="about.php">About Us</a></li>
            
            <li class="menu-item"><a href="contact.php">contact us</a></li>
             <li class="menu-item"><a href="msg.php">words of wisdom</a></li>
             <li class="menu-item"><a href="songs.php">Songs</a></li>
            <li class="menu-item"><a href="gallery.php">gallery</a></li>
            <li class="menu-item active"><a href="vids.php">video</a></li>
                    
            
            
           
            <li id="don" class="menu-item"><a href="donate.php"><img src="img/donate.png" width="150px"></a></li>
            
           
        </ul>
        <div class="nav-social-con">
            <p class="social-icons">
                <a href="https://www.facebook.com/solomojere"><i class="fa fa-facebook-square"></i></a>
                <a href="https://www.youtube.com/channel/UC4W3y9-fbPINR7FG8LINPoQ" ><i class="fa fa-youtube"></i></a>
                <a href="https://www.pinterest.com/solomonjere/"><i class="fa fa-pinterest"></i></a>
                <a href="https://twitter.com/drsolomonmusic"><i class="fa fa-twitter"></i></a>
                <a href="https://www.instagram.com/solomonjeremusic"><i class="fa fa-instagram"></i></a>
                <!--<a href="#"><i class="fa fa-linkedin-square"></i></a>-->
            </p>
            <p class="copyright-text">COPYRIGHT &copy; Solomon Jere</p>
        </div>

    </nav>
<!-- end navigation -->


    <!-- preloader area -->
    <div class="preloader-con">
        <!--<i class="fa fa-spin fa-spinner"></i>-->
        <div class="cube-preloader"></div>
    </div>
</div>


<!--
- - next will come the loaded scripts
-->

<link rel='stylesheet' property='stylesheet' type="text/css" href="dzsselector/dzsselector.css"/>
<script src="dzsselector/dzsselector.js" type="text/javascript"></script>
<script src="advancedscroller/plugin.js"></script>

<script src="qcreative/qcreative.js"></script>
<link href='advancedscroller/plugin.css' rel='stylesheet' property='stylesheet' type='text/css'>
<link href='fontawesome/font-awesome.min.css' rel='stylesheet' property='stylesheet' type='text/css'>
<script src="zfolio/zfolio.js" type="text/javascript"></script>
<script src="zfolio/jquery.isotope.min.js" type="text/javascript"></script>
<script src="dzsparallaxer/dzsparallaxer.js" type="text/javascript"></script>
<script src="dzstooltip/dzstooltip.js" type="text/javascript"></script>
<link rel='stylesheet' property='stylesheet' type="text/css" href="dzstooltip/dzstooltip.css"/>
<script src="dzsscroller/scroller.js" type="text/javascript"></script>
<link rel='stylesheet' property='stylesheet' type="text/css" href="dzsscroller/scroller.css"/>

<link rel="stylesheet" property="stylesheet" type="text/css" href="videogallery/vplayer.css"/>
<script type="text/javascript" src="videogallery/vplayer.js"></script>
<!-- customizer scripts --><!-- customizer scripts END --></body>
</html>	