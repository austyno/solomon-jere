<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>About</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
    <link rel="icon" href="img/icon2.ico" />


    <!-- - - main style scripts -->
    <link rel='stylesheet' property='stylesheet' type="text/css" href="./bootstrap/bootstrap.min.css"/>
    <link rel='stylesheet' property='stylesheet' type="text/css" href="./qcreative/qcreative.css"/>
    <link rel='stylesheet' property='stylesheet' type="text/css" href="./qcreative/include_et.css"/>
    <link rel='stylesheet' property='stylesheet' type="text/css" href="dzsparallaxer/dzsparallaxer.css"/>
    <link rel="stylesheet" type="text/css" href="css/customstyle.css" />
    <script src="js/jquery.js"></script>
    <link rel='stylesheet' property='stylesheet' type="text/css" href="zfolio/zfolio.css"/>

    <!-- font inclusions 
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:700italic,400,400italic,600,600italic,700,800' rel='stylesheet' property='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,400italic,700,900,900italic' rel='stylesheet' property='stylesheet' type='text/css'>-->
    <!-- font inclusions END-->



    <!-- preseter styles -->
    <link rel='stylesheet' property='stylesheet' type="text/css" href="dzstooltip/dzstooltip.css"/>

    <!-- preseter styles END-->


    <script class="mainoptions">

        // -- page options come here


        window.qcreative_options = {
            images_arr: ['img/backgrounds/21.jpg']  // -- the background
            ,enable_ajax: 'off' // -- enable "on" - ajax transition between pages / disable "off"
            ,bg_isparallax: 'on' // -- apply a parallax effect on scroll
//            ,new_bg_transition: "off" // -- if set to "off" then the initial background will remain
//            ,content_width: '1170' // -- apply a parallax effect on scroll
//            ,enable_native_scrollbar: 'on' // -- enable the native scrollbar on the whole site

//            ,responsive_menu_type: "select" // -- "custom" a custom forged, "select" a select menu for native mobile devices select menu

//            -- page options END here
        };
    </script>

    
    
    <style>
        #menu li a:hover{
            background-color:#0099cc;
        }
        
        #menu li ul li a{
             background-color:#0099cc;
        }
        #menu li ul li:hover > a{
            background-color:#0099cc;
        }
   
    </style>   
</head>


<body class="page-normal post-type-page content-align-center page-title-align-right page-title-style-2  menu-type-1 body-style-light">



<div class="main-container">


    <!-- this is where the main background will come, set image from above ( images_arr ) -->
    <div class="main-bg-con dzsparallaxer " data-options='{  mode_scroll: "fromtop" }'>
        <!--<figure class="main-bg dzsparallaxer&#45;&#45;target" style=""></figure>-->

        <!--<video src="video/under_the_sea_hd_stock_video.mp4" autoplay style="width: 100%;height: 100%; position: absolute;top:0;left:0; object-fit: cover;"></video>-->

    </div>




    <div class="the-content-con " style=";">



        <!-- page title // modify here -->
        <h1 class=""><i style="color:#0099cc"></i></h1>




        <div class="the-content">
            <!-- markup for blur // do not modify -->
            <div class="translucent-con ">
                <div class="translucent-bg  for-parallaxer"></div>
                <canvas class="translucent-canvas"></canvas>
                <div class="translucent-overlay"></div>
            </div>


            <div class="the-content-inner">



                <!-- section start -->

                <div class="the-content-sheet">

                    <!-- section featured media -->
                    <div class="featured-media-con">
                        <div class="featured-media--image divimage lazyloading-transition-fade-in abt" data-src="img/about/aboutimg.jpg" style=" height:400px;"></div>
                    </div>

                    <div class="the-content-sheet-text">




                        <!-- section title -->
                        <div class="row row-margin">
                            <div class="col-md-12">

                                <h2 style="color:skyblue">PROFILE</h2>
                            </div>
                        </div>
                        <div class="section-number">01</div>


                        <!-- first row of icon boxes -->
                        <div class="row row-margin">
                            <p style="font-size:18px">Dr. Solomon Jere is a Zambian gospel artist who has written his name in the fabric of the Zambian music industry. Even though he has been singing since 1994, it was the hit tracks ‘Jonah’, ‘Tisekelele’ (Let’s rejoice) and ‘Tikondane’ (featuring Skeffa Chimoto of Malawi) that brought him to limelight, and since then, there has been no turning back for him. He is the founder and owner of the record label SJ Music Label.</p>
                            <p style="font-size:18px">Dr. Solomon Jere was born into a Christian family of seven in Eastern Province of Zambia. He has six siblings. He began singing at the age of ten. He learned to play the guitar on his own when he was young. In his teenage years, he became a born-again Christian. He has 21 albums to his name. His latest album entitled ‘Wabadwa’ (Jesus is born) is “finger-lickingly” appealing.</p>
                            <p style="font-size:18px">His debut album ‘Jonah’ was released in 2000. It was a 10-track album and was distributed by SJ Music. ‘Tisekelele’ (Let’s rejoice) his second album was released in 2001 and another, ‘Tikondane’ (Let’s love one another), in 2011. He is currently known as one of Zambia's famous Gospel artistes. In 1999, he started appearing in the television broadcasts of the Zambia National Broadcasting Corporation. Besides being an artiste with many songs in gospel musical genre, he is also a genius lawyer, former Police Officer and Diplomat. </p>



                            <!-- icon box -->
                            
                        <!-- end first row of icon boxes -->




                        <!-- second row of icon boxes -->

                        


                    </div>
                </div>
                </div>
            <!-- end first section -->



            <!-- section start -->
            
            <!-- section END -->



            <!-- section start -->
            <div class="the-content-sheet ">



                <div class="the-content-sheet-text">

                    <!-- section title -->
                    <div class="row row-margin"><div class="col-md-12"><h2  style="color:skyblue">Vision</h2></div></div>
                    <div class="section-number">02</div>



                    <div class="row row-margin row-text-element">
                        
                        <div class="col-md-6">
                            <h6>MISSION</h6>
                            <hr class="qcreative-hr-small">
                            <p class="paragraph-text" style="font-size:18px">
                                To obey and listen to God as He deems it appropriate to assign me as an instrument of positive spiritual growth to all musically and otherwise. To utilize all available talents that God has blessed within and without me to His glory in song and service.</p>
                            
                                <blockquote class="blockquote-element">Sing unto him a new song, play skilfully with a loud noise.</blockquote>
                            <h6 class="blockquote-author"><i class="fa fa-long-arrow-right"></i>&nbsp;&nbsp;Psalm 33:3</h6>
                        </div>
                        
                        <div class="col-md-6">
                            <h6>VISION</h6>
                            <hr class="qcreative-hr-small">
                            <p class="paragraph-text" style="font-size:18px">To focus on the salvation path of Jesus Christ in song and service as we await His second return..</p>
                        </div>
                        
                        
                    </div>




                    <!-- row of progress lines -->
                    



                    <!-- a negative divider to make the two progress bars rows grouped -->
                    <div class="dividern50"></div>



                    

                    <!-- row of progress lines END -->



                </div>
            </div>


            




            <!-- section start -->
            <div class="the-content-sheet">



                <div class="the-content-sheet-text">

                    <!-- section title -->
                    <h2 style="color:skyblue">MEET<br>THE TEAM</h2>
                    <div class="section-number">03</div>


                    <!-- meet the team component -->
                    <div class="meet-the-team-con">
                        <div class="row row-margin">



                            <!-- team member markup -->
                            <div class="col-md-6">
                                <div class="team-member-element">
                                    <div class="pic-con">
                                        <div class="divimage" style="background-image: url(img/about/aboutimg2.jpg)"></div>
                                    </div>
                                    <div class="meta-con">
                                        <div class="first-name">Dr Solomon</div>
                                        <div class="sur-name">Jere</div>
                                        <hr class="qcreative-hr-small">
                                        <div class="the-role">Founder/Chairman</div>
                                        <div class="social-profiles">
                                            <a href="#" class="circle-con"><i class="fa fa-facebook"></i></a>
                                            <a href="#"  class="circle-con"><i class="fa fa-pinterest-p"></i></a>
                                            <a href="#"  class="circle-con"><i class="fa fa-twitter"></i></a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <!-- team member markup END -->

                            <!-- team member markup -->
                            <div class="col-md-6">
                                <div class="team-member-element">
                                    <div class="pic-con">
                                        <div class="divimage" style="background-image: url(img/about/emma2.jpg)"></div>
                                    </div>
                                    <div class="meta-con">
                                        <div class="first-name">Emma</div>
                                        <div class="sur-name">Zaki</div>
                                        <hr class="qcreative-hr-small">
                                        <div class="the-role">Manager/Music Producer/Film Maker</div>
                                        <div class="social-profiles">
                                            <a href="https://www.facebook.com/emma.zaki.3" class="circle-con"><i class="fa fa-facebook"></i></a>
                                            <a href="#"  class="circle-con"><i class="fa fa-vimeo-square"></i></a>
                                            <a href="#"  class="circle-con"><i class="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <!-- team member markup END-->
                        </div>

                        <div class="row row-margin">

                            <!-- team member markup -->
                            <div class="col-md-6">
                                <div class="team-member-element style2">
                                    <div class="pic-con">
                                        <div class="divimage" style="background-image: url(img/about/joyce.jpg)"></div>
                                    </div>
                                    <div class="meta-con">
                                        <div class="first-name">Joyce</div>
                                        <div class="sur-name">Mwanza</div>
                                        <hr class="qcreative-hr-small">
                                        <div class="the-role">Backup Singer/Song Writer</div>
                                        <div class="social-profiles">
                                            <a href="#" class="circle-con"><i class="fa fa-instagram"></i></a>
                                            <a href="#"  class="circle-con"><i class="fa fa-twitter"></i></a>
                                            <a href="#"  class="circle-con"><i class="fa fa-github"></i></a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <!-- team member markup END -->

                            <!-- team member markup -->
                            <div class="col-md-6">
                                <div class="team-member-element style2">
                                    <div class="pic-con">
                                        <div class="divimage" style="background-image: url(img/about/prince.jpg)"></div>
                                    </div>
                                    <div class="meta-con">
                                        <div class="first-name">Allan Prince</div>
                                        <div class="sur-name">Lukomba</div>
                                        <hr class="qcreative-hr-small">
                                        <div class="the-role">Music Producer</div>
                                        <div class="social-profiles">
                                            <a href="#" class="circle-con"><i class="fa fa-linkedin"></i></a>
                                            <a href="#"  class="circle-con"><i class="fa fa-twitter"></i></a>
                                            <a href="#"  class="circle-con"><i class="fa fa-dribbble"></i></a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <!-- team member markup END -->
                        </div>
                        
                        
                        
                        <div class="row row-margin">



                            <!-- team member markup -->
                            <div class="col-md-6">
                                <div class="team-member-element">
                                    <div class="pic-con">
                                        <div class="divimage" style="background-image: url(img/about/mbuzi.jpg)"></div>
                                    </div>
                                    <div class="meta-con">
                                        <div class="first-name">Temwani</div>
                                        <div class="sur-name">Mbuzi</div>
                                        <hr class="qcreative-hr-small">
                                        <div class="the-role">Singer/Song Writer</div>
                                        <div class="social-profiles">
                                            <a href="#" class="circle-con"><i class="fa fa-facebook"></i></a>
                                            <a href="#"  class="circle-con"><i class="fa fa-pinterest-p"></i></a>
                                            <a href="#"  class="circle-con"><i class="fa fa-twitter"></i></a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <!-- team member markup END -->

                            <!-- team member markup -->
                            <div class="col-md-6">
                                <div class="team-member-element">
                                    <div class="pic-con">
                                        <div class="divimage" style="background-image: url(img/about/chidinma.jpg)"></div>
                                    </div>
                                    <div class="meta-con">
                                        <div class="first-name">Chidinma </div>
                                        <div class="sur-name">Fred</div>
                                        <hr class="qcreative-hr-small">
                                        <div class="the-role">singer/song writer</div>
                                        <div class="social-profiles">
                                            <a href="https://www.facebook.com/emma.zaki.3" class="circle-con"><i class="fa fa-facebook"></i></a>
                                            <a href="#"  class="circle-con"><i class="fa fa-vimeo-square"></i></a>
                                            <a href="#"  class="circle-con"><i class="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <!-- team member markup END-->
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                    </div>




                </div>
            </div>


<div class="the-content-sheet">

                    <div class="featured-media-con social-block sc-social-block">

                        <div class="featured-media--image divimage" style="background-image: url(img/secondary_content/social.jpg);background-color:#0099cc"></div>
                        <div class="semi-black-overlay opaque"></div>
                        <div class="row">

                            <div class="col-md-3">
                                <a href="https://www.facebook.com/solomojere" class="social-meta-con">

                                    <h4>LIKE US ON</h4>
                                    <span class="social-circle-con"><i class="fa fa-facebook-square"></i></span>
                                    <div class="clear"></div>

                                    <hr>
                                    <h3>Facebook</h3>

                                </a>
                            </div>

                            <div class="col-md-3">
                                <a href="https://twitter.com/drsolomonmusic" class="social-meta-con">

                                    <h4>FOLLOW US ON</h4>
                                    <span class="social-circle-con"><i class="fa fa-twitter"></i></span>
                                    <div class="clear"></div>

                                    <hr>
                                    <h3>Twitter</h3>

                                </a>
                            </div>

                            <div class="col-md-3">
                                <a href="#" class="social-meta-con">

                                    <h4>PIN THIS ON</h4>
                                    <span class="social-circle-con"><i class="fa fa-pinterest"></i></span>
                                    <div class="clear"></div>

                                    <hr>
                                    <h3>Pinterest</h3>

                                </a>
                            </div>

                            <div class="col-md-3">
                                <a href="#" class="social-meta-con">

                                    <h4>APPRECIATE US ON</h4>
                                    <span class="social-circle-con"><i class="fa fa-google-plus"></i></span>
                                    <div class="clear"></div>

                                    <hr>
                                    <h3>Google+</h3>

                                </a>
                            </div>
                        </div>
                    </div>
                </div>























        </div>
    </div>
        </div>
    </div>






    <!-- this block is the navigation -->
    <nav class="qcreative--nav-con">
        <div class="translucent-con " style="">
            <div class="translucent-bg for-parallaxer"></div>
            <canvas id="tr" class="translucent-canvas"></canvas>
            <div class="translucent-overlay"></div>
        </div>

        <!-- modify logo from here -->
        <div class="logo-con">
            <div class="the-logo" style="width: 183px; background-image:url(img/logo3.png);"></div>
        </div>
        
        <!-- this block will be populated by gallery thumbnail navigation -->
        <ul id="menu" class="the-actual-nav">
            <li class="menu-item"><a href="index.php">Home</a></li>
            <li class="menu-item active"><a href="about.php">About Us</a></li>
            
            <li class="menu-item"><a href="contact.php">contact us</a></li>
             <li class="menu-item"><a href="msg.php">words of wisdom</a></li>
             <li class="menu-item"><a href="songs.php">Songs</a></li>
            <li class="menu-item"><a href="gallery.php">gallery</a></li>
            <li class="menu-item"><a href="vids.php">video</a></li>
                    
            
            
            
            <li id="don" class="menu-item"><a href="donate.php"><img src="img/donate.png" width="150px"></a></li>
            
           
        </ul>
        <div class="nav-social-con">
            <p class="social-icons">
                <a href="https://www.facebook.com/solomojere"><i class="fa fa-facebook-square"></i></a>
                <a href="https://www.youtube.com/channel/UC4W3y9-fbPINR7FG8LINPoQ" ><i class="fa fa-youtube"></i></a>
                <a href="https://www.pinterest.com/solomonjere/"><i class="fa fa-pinterest"></i></a>
                <a href="https://twitter.com/drsolomonmusic"><i class="fa fa-twitter"></i></a>
                <a href="https://www.instagram.com/solomonjeremusic"><i class="fa fa-instagram"></i></a>
                <!--<a href="#"><i class="fa fa-linkedin-square"></i></a>-->
            </p>
            <p class="copyright-text">COPYRIGHT &copy; Solomon Jere</p>
        </div>

    </nav>
<!-- end navigation -->


    <!-- preloader area -->
    <div class="preloader-con">
       <i class="fa fa-spin fa-spinner"></i>-->
        <div class="cube-preloader"></div>
    </div>
</div>



<!-- lightbox settings -->
<script class="zoombox-settings">
    window.init_zoombox_settings = {
        settings_zoom_doNotGoBeyond1X:'off'
        ,design_skin:'skin-whitefull'
        ,settings_enableSwipe:'on'
        ,settings_enableSwipeOnDesktop:'on'
        ,settings_galleryMenu:'dock'
        ,settings_useImageTag:'on'
        ,settings_paddingHorizontal : '0'
        ,settings_paddingVertical : '0'
    };

</script>



<!--
- - next will come the loaded scripts
-->
<link rel='stylesheet' property='stylesheet' type="text/css" href="dzsselector/dzsselector.css"/>
<script src="dzsselector/dzsselector.js" type="text/javascript"></script>

<script src="qcreative/qcreative.js"></script>
<link href='advancedscroller/plugin.css' rel='stylesheet' property='stylesheet' type='text/css'>
<script src="advancedscroller/plugin.js"></script>
<link href='fontawesome/font-awesome.min.css' rel='stylesheet' property='stylesheet' type='text/css'>
<link rel='stylesheet' property='stylesheet' type="text/css" href="zoombox/zoombox.css"/>
<script src="zoombox/zoombox.js" type="text/javascript"></script>
<script src="zfolio/zfolio.js" type="text/javascript"></script>
<script src="zfolio/jquery.isotope.min.js" type="text/javascript"></script>
<script src="dzsparallaxer/dzsparallaxer.js" type="text/javascript"></script>
<script src="dzstooltip/dzstooltip.js" type="text/javascript"></script>
<link rel='stylesheet' property='stylesheet' type="text/css" href="dzsprogressbars/dzsprogressbars.css"/>
<script src="dzsprogressbars/dzsprogressbars.js" type="text/javascript"></script>
    
<link rel='stylesheet' property='stylesheet' type="text/css" href="dzstabsandaccordions/dzstabsandaccordions.css"/>
<script src="dzstabsandaccordions/dzstabsandaccordions.js"></script>
    
    
    <link rel='stylesheet' type="text/css" href="audioplayer/audioplayer.css"/>
<script src="audioplayer/audioplayer.js" type="text/javascript"></script>
    
    
<script src="dzsscroller/scroller.js" type="text/javascript"></script>
<link rel='stylesheet' property='stylesheet' type="text/css" href="dzsscroller/scroller.css"/>
<link rel='stylesheet' property='stylesheet' type="text/css" href="videogallery/vplayer.css"/>
<script type="text/javascript" src="videogallery/vplayer.js"></script>

<!-- customizer scripts --><!-- customizer scripts END -->


</body>
</html>
 